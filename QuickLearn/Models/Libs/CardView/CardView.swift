import UIKit

@IBDesignable
class CardView: UIView {

    override func layoutSubviews() {
        self.layer.cornerRadius = 10
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.withAlphaComponent(0.65).cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 1);
        self.layer.shadowOpacity = 0.4
        self.layer.shadowRadius = 3.0
    }

}

