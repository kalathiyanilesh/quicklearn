//
//  HttpRequestManager.swift
//  Copyright © 2018 Nilesh. All rights reserved.

import UIKit
import SwiftyJSON
//import AFNetworking
import Alamofire

class HttpRequestManager
{
    static let sharedInstance = HttpRequestManager()
    var alamoFireManager = Alamofire.SessionManager.default
    let additionalHeader = ["User-Agent": "iOS"]
    
    //MARK:- GET & POST
    func requestWithJsonParam( endpointurl:String,
                                   service:String,
                                   parameters:NSDictionary,
                                   method: HTTPMethod,
                                   showLoader:Bool,
                                   responseData:@escaping  (_ error: NSError?,_ responseDict: NSDictionary?) -> Void)
    {
        if isConnectedToNetwork()
        {
            if(showLoader) { showLoaderHUD(strMessage: "") }
            print("URL : \(endpointurl) \nParam :\(convertDictionaryToJSONString(dic: parameters) ?? "not found")")
            
            ShowNetworkIndicator(xx: true)
            alamoFireManager.request(endpointurl, method: method, parameters: parameters as? Parameters, encoding: JSONEncoding.default, headers: additionalHeader)
                .responseString(completionHandler: { (responseString) in
                    //print(responseString.value ?? "error")
                    ShowNetworkIndicator(xx: false)
                    if(showLoader) { hideLoaderHUD() }
                    if(responseString.value == nil)
                    {
                        responseData(responseString.error as NSError?, nil)
                    }
                    else
                    {
                        ShowNetworkIndicator(xx: false)
                        let strResponse = "\(responseString.value!)"
                        let arr = strResponse.components(separatedBy: "\n")
                        let dict =  convertStringToDictionary(str:(arr.last  ?? "")!)
                        if dict != nil {
                            let dictResponse : NSDictionary = dictionaryOfFilteredBy(dict: dict! as NSDictionary)
                            responseData(nil,dictResponse)
                        }
                    }
                })
        }
    }
    
    func requestWithPostMultipartParam(endpointurl:String,
                                       showLoader:Bool,
                                       parameters:NSDictionary,
                                       responseData:@escaping  (_ error: NSError?,_ responseDict: NSDictionary?) -> Void)
    {
        if isConnectedToNetwork()
        {
            if(showLoader) { showLoaderHUD(strMessage: "") }
            ShowNetworkIndicator(xx: true)
            alamoFireManager.upload(multipartFormData:
                { multipartFormData in
                    for (key, value) in parameters
                    {
                        if value is Data
                        {
                            multipartFormData.append(value as! Data, withName: key as! String, fileName: "helpme.jpg", mimeType: "image/jpeg")
                        }
                        else
                        {
                            multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as! String)
                        }
                    }
            }, to: endpointurl,encodingCompletion:
                { encodingResult in
                    switch encodingResult
                    {
                    case .success(let upload, _, _):
                        upload.responseString(completionHandler:{(responseString) in
                            print(responseString.value ?? "error")
                            ShowNetworkIndicator(xx: false)
                            if(showLoader) { hideLoaderHUD() }
                            if(responseString.value == nil)
                            {
                                responseData(responseString.error as NSError?, nil)
                            }
                            else
                            {
                                let strResponse = "\(responseString.value!)"
                                let arr = strResponse.components(separatedBy: "\n")
                                let dict =  convertStringToDictionary(str:(arr.last  ?? "")!)
                                if dict != nil {
                                    let dictResponse : NSDictionary = dictionaryOfFilteredBy(dict: dict! as NSDictionary)
                                    responseData(nil,dictResponse)
                                }
                            }
                        })
                        break
                    case .failure(let encodingError):
                        print("ENCODING ERROR: ",encodingError)
                        responseData(encodingError as NSError,nil)
                    }
            })
        }
    }
}
