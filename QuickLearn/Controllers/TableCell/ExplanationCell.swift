//
//  ExplanationCell.swift
//  QuickLearn
//
//  Created by NILESH on 22/04/19.
//  Copyright © 2019 NILESH. All rights reserved.
//

import UIKit

class ExplanationCell: UITableViewCell {

    @IBOutlet weak var lblExplanation: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
