//
//  LoginVC.swift
//  QuickLearn
//
//  Created by NILESH on 06/04/19.
//  Copyright © 2019 NILESH. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit

class LoginVC: UIViewController {

    @IBOutlet weak var btnLoginWidth: NSLayoutConstraint!
    @IBOutlet weak var viewLoginX: NSLayoutConstraint!
    @IBOutlet weak var heightDetail: NSLayoutConstraint!
    @IBOutlet weak var centerY: NSLayoutConstraint!
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnClose: UIButton!
    
    var isShowBack: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if SCREENWIDTH() <= 320 {
            btnLoginWidth.constant = 100
            viewLoginX.constant = 15
            heightDetail.constant = 300
            centerY.constant = 70
        }
        btnClose.isHidden = !isShowBack
        
//        txtEmail.text = "riken@mailinator.com"
//        txtPassword.text = "123456"
    }
    
    //MARK:- UIBUTTON ACTIONS
    @IBAction func btnLogin(_ sender: Any) {
        if validateTxtFieldLength(txtEmail, withMessage: EnterEmail) &&
            validateEmailAddress(txtEmail, withMessage: EnterValidEmail) &&
            validateTxtFieldLength(txtPassword, withMessage: EnterPassword) {
            apiLogin()
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.navigationController?.popViewController(animated: false)
        }, completion: nil)
    }
    
    @IBAction func btnRegister(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: idRegisterVC)
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func btnFacebook(_ sender: Any) {
        FacebookLogin()
    }
    
    @IBAction func btnGoogle(_ sender: Any) {
        GoogleLogin()
    }
    
    @IBAction func btnForgotPass(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: idForgotPasswordVC)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- FACEBOOK LOGIN
extension LoginVC {
    func FacebookLogin() {
        self.view.endEditing(true)
        if(FBSDKAccessToken.current() == nil){
            let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
            fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) -> Void in
                if (error == nil){
                    let fbloginresult : FBSDKLoginManagerLoginResult = result!
                    if (result?.isCancelled)!{ return }
                    if error != nil {
                        print(error?.localizedDescription ?? "")
                        return
                    }
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.getFBUserData()
                    }
                }
            }
        }else{
            self.getFBUserData()
        }
    }
    
    func getFBUserData(){
        showLoaderHUD(strMessage: "")
        FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
            if (error == nil){
                print(result ?? "Not Found")
                let dictResult = result as! NSDictionary
                
                let strEmail = dictResult.object(forKey: "email") as? String ?? ""
                let strProfilePic = String(format:"https://graph.facebook.com/%@/picture?type=large",dictResult.object(forKey: "id") as? String ?? "")
                
                let parameter:NSDictionary = ["service":APISocialLogin,
                                              "request":["data":[
                                                "p_clientFirstName":dictResult.object(forKey: "first_name") as? String ?? "",
                                                "p_clientLastName":dictResult.object(forKey: "last_name") as? String ?? "",
                                                "p_cleintEmail":strEmail,
                                                "p_fbId":dictResult.object(forKey: "id") as? String ?? "",
                                                "p_gId":"",
                                                "p_clientProfile":strProfilePic,
                                                "p_deviceType":DeviceType,
                                                "p_devideToken":getDeviceToken()]]]
                self.apiSocialLogin(parameter)
                
            } else {
                hideLoaderHUD()
            }
        })
    }
}

//MARK:- GOOGLE LOGIN
extension LoginVC: GIDSignInDelegate,GIDSignInUIDelegate {
    func GoogleLogin() {
        self.view.endEditing(true)
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
            hideLoaderHUD()
        } else {
            var strProfilePic = ""
            if user.profile.hasImage {
                strProfilePic = "\(user.profile.imageURL(withDimension: 200)!)"
            }
            
            let parameter:NSDictionary = ["service":APISocialLogin,
                                          "request":["data":[
                                            "p_clientFirstName":user.profile.givenName,
                                            "p_clientLastName":user.profile.familyName,
                                            "p_cleintEmail":user.profile.email,
                                            "p_fbId":"",
                                            "p_gId":user.userID,
                                            "p_clientProfile":strProfilePic,
                                            "p_deviceType":DeviceType,
                                            "p_devideToken":getDeviceToken()]]]
            self.apiSocialLogin(parameter)
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        print("Error:",error.localizedDescription)
        hideLoaderHUD()
    }
}

//MARK:- API CALLING
extension LoginVC {
    func apiLogin() {
        self.view.endEditing(true)
        let parameter:NSDictionary = ["service":APILogin,
                                      "request":["data":[
                                        "p_cleintEmail":txtEmail.text!,
                                        "p_password":txtPassword.text!,
                                        "p_deviceType":DeviceType,
                                        "p_devideToken":getDeviceToken()]]]
        
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: SERVER_URL,
            service: APILogin,
            parameters: parameter,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                self.RedirectToScreen(responseDict)
            }
        }
    }
    
    func apiSocialLogin(_ parameter:NSDictionary) {
        self.view.endEditing(true)
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: SERVER_URL,
            service: APISocialLogin,
            parameters: parameter,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                self.RedirectToScreen(responseDict)
            }
        }
    }
    
    func RedirectToScreen(_ responseDict: NSDictionary?) {
        print(responseDict ?? "not found")
        if responseDict![kSUCCESS] as? String == "1"
        {
            let dictUserData: NSDictionary = dictionaryOfFilteredBy(dict: responseDict?.object(forKey: kDATA) as! NSDictionary)
            SaveUserData(dictUserData, token: responseDict?.object(forKey: "token") as? String ?? "", secretLogId: responseDict?.object(forKey: "secret_log_id") as? String ?? "")
            let vc = loadVC(strStoryboardId: SB_TABBAR, strVCId: idSWRevealViewController)
            self.navigationController?.pushViewController(vc, animated: true)
        } else if responseDict![kSUCCESS] as? String == "2" {
            let dictUserData: NSDictionary = dictionaryOfFilteredBy(dict: responseDict?.object(forKey: kDATA) as! NSDictionary)
            let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: idPasscodeVC) as! PasscodeVC
            vc.dictUserData = dictUserData
            vc.strToken = responseDict?.object(forKey: "token") as? String ?? ""
            vc.strSecretLogID = responseDict?.object(forKey: "secret_log_id") as? String ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            showMessage(responseDict![kMESSAGE] as! String)
        }
    }
}
