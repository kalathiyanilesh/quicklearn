//
//  RegisterVC.swift
//  QuickLearn
//
//  Created by PC on 08/04/19.
//  Copyright © 2019 NILESH. All rights reserved.
//

import UIKit

class RegisterVC: UIViewController {

    @IBOutlet weak var lblRegisterY: NSLayoutConstraint!
    @IBOutlet weak var viewLoginRegBottom: NSLayoutConstraint!
    
    @IBOutlet weak var txtFName: UITextField!
    @IBOutlet weak var txtLName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhoneNo: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfPass: UITextField!
    @IBOutlet weak var txtRefrenceCode: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if SCREENWIDTH() <= 320 {
            lblRegisterY.constant = 10
            viewLoginRegBottom.constant = 10
        }
    }

    //MARK:- UIBUTTON ACTIONS
    @IBAction func btnLogin(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnRegister(_ sender: Any) {
        if validateTxtFieldLength(txtFName, withMessage: EnterFname) &&
            validateTxtFieldLength(txtLName, withMessage: EnterLname) &&
            validateTxtFieldLength(txtEmail, withMessage: EnterEmail) &&
            validateEmailAddress(txtEmail, withMessage: EnterValidEmail) &&
            validateTxtFieldLength(txtPhoneNo, withMessage: EnterPhoneNo) &&
            validatePhoneNo(txtPhoneNo, withMessage: EnterValidPhoneNo) &&
            passwordMismatch(txtPassword, txtConfPass, withMessage: PasswordMismatch) {
            apiRegister()
        }
    }
}

//MARK:- API CALLING
extension RegisterVC {
    func apiRegister() {
        self.view.endEditing(true)
        let parameter:NSDictionary = ["service":APIRegister,
                                      "request":["data":[
                                        "p_clientFirstName":txtFName.text!,
                                        "p_clientMiddleName":"",
                                        "p_clientLastName":txtLName.text!,
                                        "p_cleintEmail":txtEmail.text!,
                                        "p_password":txtPassword.text!,
                                        "p_mobileNo":txtPhoneNo.text!,
                                        "p_referenceCode":txtRefrenceCode.text ?? "",
                                        "p_deviceType":DeviceType,
                                        "p_devideToken":getDeviceToken()]]]

        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: SERVER_URL,
            service: APIRegister,
            parameters: parameter,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "not found")
                if responseDict![kSUCCESS] as? String == "1"
                {
                    let dictUserData: NSDictionary = dictionaryOfFilteredBy(dict: responseDict?.object(forKey: kDATA) as! NSDictionary)
                    let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: idPasscodeVC) as! PasscodeVC
                    vc.dictUserData = dictUserData
                    vc.strToken = responseDict?.object(forKey: "token") as? String ?? ""
                    vc.strSecretLogID = responseDict?.object(forKey: "secret_log_id") as? String ?? ""
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    showMessage(responseDict![kMESSAGE] as! String)
                }
            }
        }
    }
}
