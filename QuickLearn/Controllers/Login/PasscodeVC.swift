//
//  PasscodeVC.swift
//  QuickLearn
//
//  Created by PC on 08/04/19.
//  Copyright © 2019 NILESH. All rights reserved.
//

import UIKit

protocol MyTextFieldDelegate {
    func textFieldDidDelete()
}

class MyTextField: UITextField {
    var myDelegate: MyTextFieldDelegate?
    
    override func deleteBackward() {
        super.deleteBackward()
        myDelegate?.textFieldDidDelete()
    }
}

class PasscodeVC: UIViewController, MyTextFieldDelegate {

    @IBOutlet var first: MyTextField!
    @IBOutlet var second: MyTextField!
    @IBOutlet var third: MyTextField!
    @IBOutlet var fourth: MyTextField!
    @IBOutlet var fifth: MyTextField!
    
    var currentTextFiled:MyTextField! = nil
    var strToken: String = ""
    var strSecretLogID: String = ""
    
    var dictUserData: NSDictionary = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let strPassCode = dictUserData.object(forKey: "mobileLatestCode") as! String
        if strPassCode.count == 5 {
            first.text = String(describing: strPassCode.character(at: 0)!)
            second.text = String(describing: strPassCode.character(at: 1)!)
            third.text = String(describing: strPassCode.character(at: 2)!)
            fourth.text = String(describing: strPassCode.character(at: 3)!)
            fifth.text = String(describing: strPassCode.character(at: 4)!)
        }
        setUpTextField()
    }
    
    @IBAction func btnDone(_ sender: Any) {
        apiVerifyOTP()
    }
    
    @IBAction func btnResend(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension PasscodeVC: UITextFieldDelegate {
    func setUpTextField() {

        first.myDelegate = self
        second.myDelegate = self
        third.myDelegate = self
        fourth.myDelegate = self
        fifth.myDelegate = self
        
        first.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        second.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        third.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        fourth.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        fifth.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        
        first.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        second.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        third.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        fourth.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        fifth.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        
        currentTextFiled = first
    }
    
    func textFieldDidDelete() {
        if(currentTextFiled == first)
        {
            if(first.text!.count > 0)
            {
                first.text = ""
                currentTextFiled = first
            }
            else
            {
                self.view.endEditing(true)
            }
        }
        else if(currentTextFiled == second)
        {
            if(second.text!.count > 0)
            {
                second.text = ""
            }
            else
            {
                first.text = ""
                first.becomeFirstResponder()
            }
        }
        else if(currentTextFiled == third)
        {
            if(third.text!.count > 0)
            {
                third.text = ""
            }
            else
            {
                currentTextFiled = second
                second.text = ""
                second.becomeFirstResponder()
            }
        }
        else if(currentTextFiled == fourth)
        {
            if(fourth.text!.count > 0)
            {
                fourth.text = ""
            }
            else
            {
                currentTextFiled = third
                third.text = ""
                third.becomeFirstResponder()
            }
        }
        else if(currentTextFiled == fifth)
        {
            if(fifth.text!.count > 0)
            {
                fifth.text = ""
            }
            else
            {
                currentTextFiled = fourth
                fourth.text = ""
                fourth.becomeFirstResponder()
            }
        }
    }
    
    
    @objc func myTargetFunction(textField: UITextField) {
        if(textField == first)
        {
            currentTextFiled = first
            first.text = ""
        }
        else if(textField == second)
        {
            currentTextFiled = second
            second.text = ""
        }
        else if(textField == third)
        {
            currentTextFiled = third
            third.text = ""
        }
        else if(textField == fourth)
        {
            currentTextFiled = fourth
            fourth.text = ""
        }
        else if(textField == fifth)
        {
            currentTextFiled = fifth
            fifth.text = ""
        }
    }
    
    @objc func textFieldDidChange(textField: UITextField) {
        let text = textField.text
        if text?.count == 1 {
            switch textField{
            case first:
                currentTextFiled = second
                second.text = ""
                second.becomeFirstResponder()
            case second:
                currentTextFiled = third
                third.text = ""
                third.becomeFirstResponder()
            case third:
                currentTextFiled = fourth
                fourth.text = ""
                fourth.becomeFirstResponder()
            case fourth:
                currentTextFiled = fifth
                fifth.text = ""
                fifth.becomeFirstResponder()
            case fifth:
                self.view.endEditing(true)
            default:
                break
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        currentTextFiled = textField as? MyTextField
        textField.text = ""
    }
    
    //MARK:- TextField Delegate
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textFieldDidEndEditing(textField)
        return true
    }
}

//MARK:- API CALLING
extension PasscodeVC {
    func apiVerifyOTP() {
        self.view.endEditing(true)
        let code = "\(first.text!)" + "\(second.text!)" + "\(third.text!)" + "\(fourth.text!)" + "\(fifth.text!)"
        let parameter:NSDictionary = ["service":APIVerifyOTP,
                                      "request":["data":[
                                        "p_clientId":dictUserData.object(forKey: "clientId") as! String,
                                        "p_mobileLatestCode":code]]]
        
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: SERVER_URL,
            service: APIVerifyOTP,
            parameters: parameter,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "not found")
                if responseDict![kSUCCESS] as? String == "1"
                {
                    SaveUserData(self.dictUserData, token: self.strToken, secretLogId: self.strSecretLogID)
                    let vc = loadVC(strStoryboardId: SB_TABBAR, strVCId: idSWRevealViewController)
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    showMessage(responseDict![kMESSAGE] as! String)
                }
            }
        }
    }
}


extension StringProtocol where IndexDistance == Int {
    func index(at offset: Int, from start: Index? = nil) -> Index? {
        return index(start ?? startIndex, offsetBy: offset, limitedBy: endIndex)
    }
    func character(at offset: Int) -> Character? {
        precondition(offset >= 0, "offset can't be negative")
        guard let index = index(at: offset) else { return nil }
        return self[index]
    }
    subscript(_ range: CountableRange<Int>) -> SubSequence {
        precondition(range.lowerBound >= 0, "lowerBound can't be negative")
        let start = index(at: range.lowerBound) ?? endIndex
        let end = index(at: range.count, from: start) ?? endIndex
        return self[start..<end]
    }
    subscript(_ range: CountableClosedRange<Int>) -> SubSequence {
        precondition(range.lowerBound >= 0, "lowerBound can't be negative")
        let start = index(at: range.lowerBound) ?? endIndex
        let end = index(at: range.count, from: start) ?? endIndex
        return self[start..<end]
    }
    subscript(_ range: PartialRangeUpTo<Int>) -> SubSequence {
        return prefix(range.upperBound)
    }
    subscript(_ range: PartialRangeThrough<Int>) -> SubSequence {
        return prefix(range.upperBound+1)
    }
    subscript(_ range: PartialRangeFrom<Int>) -> SubSequence {
        return suffix(Swift.max(0,count-range.lowerBound))
    }
}
extension Substring {
    var string: String { return String(self) }
}

