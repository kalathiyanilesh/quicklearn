//
//  ForgotPasswordVC.swift
//  QuickLearn
//
//  Created by PC on 08/04/19.
//  Copyright © 2019 NILESH. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {

    @IBOutlet weak var txtEmail: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubmit(_ sender: Any) {
        if (validateTxtFieldLength(txtEmail, withMessage: EnterEmail) &&
            validateEmailAddress(txtEmail, withMessage: EnterValidEmail)) {
            apiForgotPass()
        }
    }
}

//MARK:- API CALLING
extension ForgotPasswordVC {
    func apiForgotPass() {
        self.view.endEditing(true)
        self.view.endEditing(true)
        let parameter:NSDictionary = ["service":APIForgot,
                                      "request":["data":["p_cleintEmail":txtEmail.text!]]]
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: SERVER_URL,
            service: APIForgot,
            parameters: parameter,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "not found")
                showMessage(responseDict![kMESSAGE] as! String)
                if responseDict![kSUCCESS] as? String == "1"
                {
                   self.btnBack(UIButton())
                }
            }
        }
    }
}
