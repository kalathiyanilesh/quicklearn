//
//  SelectModeVC.swift
//  QuickLearn
//
//  Created by MOJAVE on 04/06/19.
//  Copyright © 2019 NILESH. All rights reserved.
//

import UIKit

protocol delegateMode {
    func SetSelectedMode(_ type: Int, object: NSDictionary)
}

class SelectModeVC: UIViewController {

    @IBOutlet weak var btnLearnMode: UIButton!
    @IBOutlet weak var btnSkillTest: UIButton!
    
    var ModeType:Int = 0
    var delegateMode: delegateMode?
    var dictSubject = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ModeType = GetSelectedMode(dictSubject["subjectId"] as! String)
        if ModeType == QAMode.LEARNINGMODE.rawValue {
            btnLearnMode(btnLearnMode)
        } else {
            btnSkillTest(btnSkillTest)
        }
    }
    
    //MARK:- UIButton Action
    @IBAction func btnLearnMode(_ sender: Any) {
        ModeType = 0
        btnLearnMode.setTitleColor(UIColor.black, for: .normal)
        btnSkillTest.setTitleColor(UIColor.black.withAlphaComponent(0.2), for: .normal)
    }
    
    @IBAction func btnSkillTest(_ sender: Any) {
        ModeType = 1
        btnLearnMode.setTitleColor(UIColor.black.withAlphaComponent(0.2), for: .normal)
        btnSkillTest.setTitleColor(UIColor.black, for: .normal)
    }
    
    @IBAction func btnDone(_ sender: Any) {
        self.delegateMode?.SetSelectedMode(ModeType, object: dictSubject)
        DismissView()
    }
    
    @IBAction func btnClose(_ sender: Any) {
        DismissView()
    }
    
    func DismissView() {
        UIView.transition(with:APP_DELEGATE.window!, duration:0.3, options:.transitionCrossDissolve, animations: {
            self.dismiss(animated: false, completion: nil)
        }, completion:nil)
    }
}
