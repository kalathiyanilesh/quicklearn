//
//  SubjectListVC.swift
//  QuickLearn
//
//  Created by NILESH on 25/04/19.
//  Copyright © 2019 NILESH. All rights reserved.
//

import UIKit

class SubjectListCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTotalItem: UILabel!
    @IBOutlet weak var progressbar: UIProgressView!
    @IBOutlet weak var imgLeft: UIImageView!
}

class SubjectListVC: UIViewController {

    @IBOutlet weak var tblSubjectList: UITableView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgLock: UIImageView!
    
    var arrSubjectList: NSMutableArray = NSMutableArray()
    var dictCourse: NSDictionary = NSDictionary()
    var isShowProgress: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(dictCourse)
        lblTitle.text = dictCourse.object(forKey: "courseName") as? String
        
        if isShowProgress {
            activity.stopAnimating()
            arrSubjectList = (dictCourse.object(forKey: "subject") as! NSArray).mutableCopy() as! NSMutableArray
            imgLock.isHidden = true
        } else {
            if dictCourse.object(forKey: "is_purchase") as! String == "1" {
                imgLock.isHidden = true
            }
            apiGetSubjectList()
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK:- UITableViewDelegate METHOD
extension SubjectListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSubjectList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : SubjectListCell = tableView.dequeueReusableCell(withIdentifier: "SubjectListCell", for: indexPath) as! SubjectListCell
        let dictSubject = arrSubjectList[indexPath.row] as! NSDictionary
        let TotalQuestion = dictSubject.object(forKey: "totalquestion") as? String ?? "0"
        cell.lblTitle.text = dictSubject["subjectName"] as? String
        cell.lblTotalItem.text = "\(TotalQuestion) Item"
        if isShowProgress {
            cell.imgLeft.isHidden = true
        }
        if Int(TotalQuestion)! == 0 {
            cell.progressbar.progress = 0
        } else {
            let attempt = dictSubject.object(forKey: "totalattempt") as? String ?? "0"
            cell.progressbar.setProgress(Float(Int(attempt)! * 100 / Int(TotalQuestion)!) / 100, animated: true)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isShowProgress { return }
        if dictCourse.object(forKey: "is_purchase") as! String == "1" {
            let dictSubject = arrSubjectList[indexPath.row] as! NSDictionary
            if dictSubject["isActive"] as? String == "1" {
                let sSubjectID = dictSubject["subjectId"] as! String
                if isModeSelected(sSubjectID) {
                    GoForQuestion(dictSubject)
                } else {
                    let vc = loadVC(strStoryboardId: SB_Alerts, strVCId: idSelectModeVC) as! SelectModeVC
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.dictSubject = dictSubject
                    vc.delegateMode = self
                    UIView.transition(with:APP_DELEGATE.window!, duration:0.3, options:.transitionCrossDissolve, animations: {
                        self.present(vc, animated: false, completion: nil)
                    }, completion:nil)
                }
            }
        } else {
            let vc = loadVC(strStoryboardId: SB_TABBAR, strVCId: idPaymentInfoVC) as! PaymentInfoVC
            vc.dictCourse = dictCourse
            UIView.transition(with:APP_DELEGATE.window!, duration:0.3, options:.transitionCrossDissolve, animations: {
                self.navigationController?.pushViewController(vc, animated: false)
            }, completion:nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
}

//MARK:- SET APP MODE
extension SubjectListVC: delegateMode {
    func SetSelectedMode(_ type: Int, object: NSDictionary) {
        SaveSelectedMode(type, object.object(forKey: "subjectId") as! String)
        GoForQuestion(object)
    }
    
    func GoForQuestion(_ object: NSDictionary) {
        let vc = loadVC(strStoryboardId: SB_Question, strVCId: idQuestionVC) as! QuestionVC
        vc.dictSubject = object
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- API CALLING
extension SubjectListVC {
    func apiGetSubjectList() {
        self.activity.startAnimating()
        let parameter:NSDictionary = ["service":APIGetSubject,
                                      "request":["data":["course_id":dictCourse.object(forKey: "courseId") as! String]],
                                      "auth":[
                                        "id":getUserID(),
                                        "token":GetAPIToken()]]
        
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: SERVER_URL,
            service: APIGetSubject,
            parameters: parameter,
            method: .post,
            showLoader: false)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "not found")
                if responseDict![kSUCCESS] as? String == "1"
                {
                    self.arrSubjectList = arrayOfFilteredBy(arr: responseDict?.object(forKey: kDATA) as! NSArray).mutableCopy() as! NSMutableArray
                    self.tblSubjectList.reloadData()
                } else {
                    showMessage(responseDict![kMESSAGE] as! String)
                }
            }
            self.activity.stopAnimating()
        }
    }
}
