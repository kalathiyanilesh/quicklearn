//
//  SubmitedAnsVC.swift
//  QuickLearn
//
//  Created by MOJAVE on 04/05/19.
//  Copyright © 2019 NILESH. All rights reserved.
//

import UIKit
import RichTextView

class TextCell: UITableViewCell {
    @IBOutlet weak var viewQuestion: RichTextView!
    @IBOutlet weak var imgIsCorrect: UIImageView!
    @IBOutlet weak var lblQNo: UILabel!
}

class ImageCell: UITableViewCell {
    @IBOutlet weak var imgQuestion: UIImageView!
    @IBOutlet weak var imgIsCorrect: UIImageView!
    @IBOutlet weak var lblQNo: UILabel!
}

class TextImageCell: UITableViewCell {
    @IBOutlet weak var viewQuestion: RichTextView!
    @IBOutlet weak var imgQuestion: UIImageView!
    @IBOutlet weak var imgIsCorrect: UIImageView!
    @IBOutlet weak var lblQNo: UILabel!
}

class SubmitedAnsVC: UIViewController {

    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    
    var isNormalBack: Bool = false
    var arrData = NSMutableArray()
    var dictSubject = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = dictSubject.object(forKey: "subjectName") as? String
        apiGetResult()
    }
    
    //MARK:-  UIBUTTON ACTION
    @IBAction func btnBack(_ sender: Any) {
        if isNormalBack {
            self.navigationController?.popViewController(animated: true)
        } else {
            popBack(self.navigationController, toControllerType: SWRevealViewController.self)
        }
    }
}

//MARK:- UITableViewDelegate METHOD
extension SubmitedAnsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dictData = arrData.object(at: indexPath.row) as! NSDictionary
        
        let strQuestion = dictData.object(forKey: "questionText") as! String
        let strQuesImg = dictData.object(forKey: "imgPath") as! String
        let strIsCorrect = dictData.object(forKey: "isCorrectAnswer") as! String
        let strIsSkip = dictData.object(forKey: "skip") as! String
        
        var strType = ""
        if strQuestion.count > 0 && strQuesImg.count > 0 {
            strType = QAType.QuestionTextImage.rawValue
        }
        if strQuestion.count > 0 && strQuesImg.count <= 0 {
            strType = QAType.QuestionText.rawValue
        }
        if strQuesImg.count > 0 && strQuestion.count <= 0 {
            strType = QAType.QuestionImage.rawValue
        }
        
        switch strType {
        case QAType.QuestionTextImage.rawValue:
            let cell : TextImageCell = tableView.dequeueReusableCell(withIdentifier: "TextImageCell", for: indexPath) as! TextImageCell
            
            cell.lblQNo.text = "\(indexPath.row+1)."
            
            cell.viewQuestion.update(input: strQuestion,
                                     latexParser: LatexParser(),
                                     font: FONT_REGULAR(fontSize: 17),
                                     textColor: Color_Hex(hex: "0F377F"),
                                     latexTextBaselineOffset: 0,
                                     completion: nil)
            
            cell.imgQuestion.sd_setImage(with: URL(string: strQuesImg), placeholderImage: QUES_PLACE_HOLDER)
            
            if strIsSkip == "1" {
                cell.imgIsCorrect.image = UIImage(named: "ic_skip")
            } else if strIsCorrect == "1" {
              cell.imgIsCorrect.image = UIImage(named: "ic_correct")
            } else {
                cell.imgIsCorrect.image = UIImage(named: "ic_wrong")
            }
            
            return cell
        case QAType.QuestionText.rawValue:
            let cell : TextCell = tableView.dequeueReusableCell(withIdentifier: "TextCell", for: indexPath) as! TextCell
            
            cell.lblQNo.text = "\(indexPath.row+1)."
            
            cell.viewQuestion.update(input: strQuestion,
                                 latexParser: LatexParser(),
                                 font: FONT_REGULAR(fontSize: 17),
                                 textColor: Color_Hex(hex: "0F377F"),
                                 latexTextBaselineOffset: 0,
                                 completion: nil)
            
            if strIsSkip == "1" {
                cell.imgIsCorrect.image = UIImage(named: "ic_skip")
            } else if strIsCorrect == "1" {
              cell.imgIsCorrect.image = UIImage(named: "ic_correct")
            } else {
                cell.imgIsCorrect.image = UIImage(named: "ic_wrong")
            }
            
            return cell
        case QAType.QuestionImage.rawValue:
            let cell : ImageCell = tableView.dequeueReusableCell(withIdentifier: "ImageCell", for: indexPath) as! ImageCell
            
            cell.lblQNo.text = "\(indexPath.row+1)."
            
            cell.imgQuestion.sd_setImage(with: URL(string: strQuesImg), placeholderImage: QUES_PLACE_HOLDER)
            
            if strIsSkip == "1" {
                cell.imgIsCorrect.image = UIImage(named: "ic_skip")
            } else if strIsCorrect == "1" {
              cell.imgIsCorrect.image = UIImage(named: "ic_correct")
            } else {
                cell.imgIsCorrect.image = UIImage(named: "ic_wrong")
            }
            
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = loadVC(strStoryboardId: SB_Question, strVCId: idExplanationVC) as! ExplanationVC
        vc.dictQuestion = arrData.object(at: indexPath.row) as! NSDictionary
        vc.strTitle = dictSubject.object(forKey: "subjectName") as! String
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let dictData = arrData.object(at: indexPath.row) as! NSDictionary
        let strQuestion = dictData.object(forKey: "questionText") as! String
        let strQuesImg = dictData.object(forKey: "imgPath") as! String
        if strQuesImg.count > 0 && strQuestion.count <= 0 {
            return 250
        } else {
            return UITableViewAutomaticDimension
        }
    }
}

//MARK:- API CALLING
extension SubmitedAnsVC {
    func apiGetResult() {
        let parameter:NSDictionary = ["service":APIGetResult,
                                      "request":["data":[
                                        "p_courseId":dictSubject.object(forKey: "courseId") as! String,
                                        "p_subjectId":dictSubject.object(forKey: "subjectId") as! String]],
                                      "auth":[
                                        "id":getUserID(),
                                        "token":GetAPIToken()]]
        
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: SERVER_URL,
            service: APIGetResult,
            parameters: parameter,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "not found")
                if responseDict![kSUCCESS] as? String == "1"
                {
                    self.arrData = (responseDict?.object(forKey: kDATA) as! NSArray).mutableCopy() as! NSMutableArray
                    self.tblList.reloadData()
                }
            }
        }
    }
}
