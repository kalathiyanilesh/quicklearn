
//
//  ExplanationVC.swift
//  QuickLearn
//
//  Created by MOJAVE on 04/05/19.
//  Copyright © 2019 NILESH. All rights reserved.
//

import UIKit
import RichTextView
import URLEmbeddedView
import SafariServices
import WebKit
import Lightbox

class ExplanationTextCell: UITableViewCell {
    @IBOutlet weak var vwWeb: WKWebView!
    @IBOutlet weak var viewSubscribe: UIView!
    @IBOutlet weak var btnSubscription: UIButton!
}

class ExplanationImageCell: UITableViewCell {
    @IBOutlet weak var imgExplanation: UIImageView!
    @IBOutlet weak var viewSubscribe: UIView!
    @IBOutlet weak var btnSubscription: UIButton!
    @IBOutlet weak var btnViewFull: UIButton!
}

class ExplanationTextImageCell: UITableViewCell {
    @IBOutlet weak var vwWeb: WKWebView!
    @IBOutlet weak var imgExplanation: UIImageView!
    @IBOutlet weak var viewSubscribe: UIView!
    @IBOutlet weak var btnSubscription: UIButton!
    @IBOutlet weak var btnViewFull: UIButton!
}

class ExplanationPlayButton: UITableViewCell {
    @IBOutlet weak var btnPlay: UIButton!
}

protocol delegateExplain {
    func GoForNextQuestion()
}

class ExplanationVC: UIViewController {
    
    @IBOutlet var viewWrong: UIView!
    @IBOutlet var viewRight: UIView!
    @IBOutlet var viewSkip: UIView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var tblExp: UITableView!
    @IBOutlet var bottomTbl: NSLayoutConstraint!
    @IBOutlet var btnNext: UIButton!
    
    var delegateExplain: delegateExplain?
    var isDirectFromQues: Bool = false
    var dictQuestion = NSDictionary()
    var arrCellType = NSMutableArray()
    var arrSelectedAnsID = NSMutableArray()
    var strTitle = ""
    var heightExplain: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(dictQuestion)
        tblExp.register(UINib(nibName: "LinkViewCell", bundle: nil), forCellReuseIdentifier: "LinkViewCell")
        SetUpData()
        if !isDirectFromQues {
            bottomTbl.constant = 0
            btnNext.isHidden = true
        }
    }
    
    //MARK:- UIButton Action
    @IBAction func btnBack(_ sender: Any) {
        if isDirectFromQues {
            self.navigationController?.popViewController(animated: true)
            self.delegateExplain?.GoForNextQuestion()
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
}

//MARK:- UITableViewDelegate METHOD
extension ExplanationVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCellType.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dictCell = arrCellType.object(at: indexPath.row) as! NSDictionary
        let strType = dictCell.object(forKey: "type") as! String
        
        switch strType {
            
            
        //============ QuestionText ============
        case QAType.QuestionText.rawValue:
            let cell : QuestionListCell = tableView.dequeueReusableCell(withIdentifier: "QuestionListCell", for: indexPath) as! QuestionListCell
            cell.viewQuestion.update(input: dictCell.object(forKey: "questionText") as? String ?? "",
                                     latexParser: LatexParser(),
                                     font: FONT_REGULAR(fontSize: 17),
                                     textColor: Color_Hex(hex: "0F377F"),
                                     latexTextBaselineOffset: 0,
                                     completion: nil)
            return cell
            
            
        //============ QuestionImage ============
        case QAType.QuestionImage.rawValue:
            let cell : QuesImageCell = tableView.dequeueReusableCell(withIdentifier: "QuesImageCell", for: indexPath) as! QuesImageCell
            cell.imgQuestion.sd_setImage(with: URL(string: dictCell.object(forKey: "questionImg") as? String ?? ""), placeholderImage: QUES_PLACE_HOLDER)
            return cell
            
            
        //============ QuestionTextImage ============
        case QAType.QuestionTextImage.rawValue:
            let cell : QuesTextImageCell = tableView.dequeueReusableCell(withIdentifier: "QuesTextImageCell", for: indexPath) as! QuesTextImageCell
            cell.imgQuestion.sd_setImage(with: URL(string: dictCell.object(forKey: "questionImg") as? String ?? ""), placeholderImage: QUES_PLACE_HOLDER)
            cell.viewQuestion.update(input: dictCell.object(forKey: "questionText") as? String ?? "",
                                     latexParser: LatexParser(),
                                     font: FONT_REGULAR(fontSize: 17),
                                     textColor: Color_Hex(hex: "0F377F"),
                                     latexTextBaselineOffset: 0,
                                     completion: nil)
            return cell
            
            
        //============ AnswerText ============
        case QAType.AnswerText.rawValue:
            let cell : AnswerListCell = tableView.dequeueReusableCell(withIdentifier: "AnswerListCell", for: indexPath) as! AnswerListCell
            cell.viewAnswer.update(input: dictCell.object(forKey: "answerText") as? String ?? "",
                                   latexParser: LatexParser(),
                                   font: FONT_REGULAR(fontSize: 17),
                                   textColor: Color_Hex(hex: "0F377F"),
                                   latexTextBaselineOffset: 0,
                                   completion: nil)
            setColor(dictCell, cell.imgSelection, cell.viewBack)
            return cell
            
            
        //============ AnswerImage ============
        case QAType.AnswerImage.rawValue:
            let cell : AnsImageCell = tableView.dequeueReusableCell(withIdentifier: "AnsImageCell", for: indexPath) as! AnsImageCell
            cell.imgAnswer.sd_setImage(with: URL(string: dictCell.object(forKey: "questionImg") as? String ?? ""), placeholderImage: QUES_PLACE_HOLDER)
            setColor(dictCell, cell.imgSelection, cell.viewBack)
            return cell
            
            
        //============ AnswerTextImage ============
        case QAType.AnswerTextImage.rawValue:
            let cell : AnsTextImageCell = tableView.dequeueReusableCell(withIdentifier: "AnsTextImageCell", for: indexPath) as! AnsTextImageCell
            cell.viewAnswer.update(input: dictCell.object(forKey: "answerText") as? String ?? "",
                                   latexParser: LatexParser(),
                                   font: FONT_REGULAR(fontSize: 17),
                                   textColor: Color_Hex(hex: "0F377F"),
                                   latexTextBaselineOffset: 0,
                                   completion: nil)
            cell.imgAnswer.sd_setImage(with: URL(string: dictCell.object(forKey: "answerImg") as? String ?? ""), placeholderImage: QUES_PLACE_HOLDER)
            setColor(dictCell, cell.imgSelection, cell.viewBack)
            return cell
            
        //============ ExplainTitle ============
        case QAType.ExplainTitle.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Explanation", for: indexPath) as! ExplanationPlayButton
            return cell
            
        //============ ExplainText ============
        case QAType.ExplainText.rawValue:
            let cell : ExplanationTextCell = tableView.dequeueReusableCell(withIdentifier: "ExplanationTextCell", for: indexPath) as! ExplanationTextCell
            if heightExplain == 0 {
                loadLatexString(cell.vwWeb, dictCell.object(forKey: "explanationText") as? String ?? "")
            }
            cell.viewSubscribe.isHidden = true
            if !cell.viewSubscribe.isHidden {
                cell.btnSubscription.addTarget(self, action: #selector(btnSubscription(_ :)), for: .touchUpInside)
            }
            return cell
            
            
        //============ ExplainImage ============
        case QAType.ExplainImage.rawValue:
            let cell : ExplanationImageCell = tableView.dequeueReusableCell(withIdentifier: "ExplanationImageCell", for: indexPath) as! ExplanationImageCell
            cell.imgExplanation.sd_setImage(with: URL(string: dictCell.object(forKey: "explanationImg") as? String ?? ""), placeholderImage: QUES_PLACE_HOLDER)
            cell.viewSubscribe.isHidden = true
            if !cell.viewSubscribe.isHidden {
                cell.btnSubscription.addTarget(self, action: #selector(btnSubscription(_ :)), for: .touchUpInside)
            }
            cell.btnViewFull.layer.cornerRadius = 5
            cell.btnViewFull.clipsToBounds = true
            cell.btnViewFull.addTarget(self, action: #selector(btnViewFull(_ :)), for: .touchUpInside)
            return cell
            
            
        //============ ExplainTextImage ============
        case QAType.ExplainTextImage.rawValue:
            let cell : ExplanationTextImageCell = tableView.dequeueReusableCell(withIdentifier: "ExplanationTextImageCell", for: indexPath) as! ExplanationTextImageCell
            cell.imgExplanation.sd_setImage(with: URL(string: dictCell.object(forKey: "explanationImg") as? String ?? ""), placeholderImage: QUES_PLACE_HOLDER)
            if heightExplain == 0 {
                loadLatexString(cell.vwWeb, dictCell.object(forKey: "explanationText") as? String ?? "")
            }
            cell.viewSubscribe.isHidden = true
            if !cell.viewSubscribe.isHidden {
                cell.btnSubscription.addTarget(self, action: #selector(btnSubscription(_ :)), for: .touchUpInside)
            }
            cell.btnViewFull.layer.cornerRadius = 5
            cell.btnViewFull.clipsToBounds = true
            cell.btnViewFull.addTarget(self, action: #selector(btnViewFull(_ :)), for: .touchUpInside)
            return cell
            
            
        //============ ExplainVideoLink ============
        case QAType.ExplainVideo.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: "LinkViewCell", for: indexPath) as! LinkViewCell
            let urlString = dictCell.object(forKey: "explanationImgVideoPath") as? String ?? ""
            if cell.embeddedView != nil {
                cell.embeddedView.removeFromSuperview()
            }
            
            cell.embeddedView = URLEmbeddedView.init()
            cell.embeddedView.frame = cell.vwLinkAddress.bounds
            cell.vwLinkAddress.addSubview(cell.embeddedView)
            
            cell.embeddedView.load(urlString: urlString)
            cell.embeddedView.didTapHandler = { [weak self] embeddedView, URL in
                guard let URL = URL else { return }
                if #available(iOS 9.0, *) {
                    self?.present(SFSafariViewController(url: URL), animated: true, completion: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
            
            cell.viewSubscribe.isHidden = true
            
            return cell
            
        //============ ExplainPlayButton ============
        case QAType.ExplainPlayButton.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ExplanationPlayButton", for: indexPath) as! ExplanationPlayButton
            cell.btnPlay.addTarget(self, action: #selector(btnPlay(_ :)), for: .touchUpInside)
            return cell
        
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let dictCell = arrCellType.object(at: indexPath.row) as! NSDictionary
        let strType = dictCell.object(forKey: "type") as! String
        switch strType {
        case QAType.QuestionImage.rawValue, QAType.AnswerImage.rawValue, QAType.ExplainImage.rawValue:
            return 250
        case QAType.ExplainText.rawValue:
            return heightExplain
        case QAType.ExplainTextImage.rawValue:
            return heightExplain+210+28
        case QAType.ExplainVideo.rawValue:
            return 120
        case QAType.ExplainPlayButton.rawValue:
            return 50
        case QAType.ExplainTitle.rawValue:
            return 70
        default:
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    
    @objc func btnSubscription(_ sender: UIButton!) {
        let buttonPosition = sender.convert(CGPoint.zero, to: tblExp)
        let indexPath = tblExp.indexPathForRow(at: buttonPosition)!
        let dictCell = arrCellType.object(at: indexPath.row) as! NSDictionary
        print(dictCell)
        let vc = loadVC(strStoryboardId: SB_TABBAR, strVCId: idPaymentVC) as! PaymentVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func btnViewFull(_ sender: UIButton!) {
        let buttonPosition = sender.convert(CGPoint.zero, to: tblExp)
        let indexPath = tblExp.indexPathForRow(at: buttonPosition)!
        let dictCell = arrCellType.object(at: indexPath.row) as! NSDictionary
        print(dictCell)
        let strImage = dictCell.object(forKey: "explanationImg") as? String ?? ""
        let images = [
          LightboxImage(imageURL: URL(string: strImage)!)
        ]

        // Create an instance of LightboxController.
        let controller = LightboxController(images: images)

        // Use dynamic background.
        controller.dynamicBackground = true

        controller.modalPresentationStyle = .fullScreen
        // Present your controller.
        present(controller, animated: true, completion: nil)
    }
    
    @objc func btnPlay(_ sender: UIButton!) {
        let buttonPosition = sender.convert(CGPoint.zero, to: tblExp)
        let indexPath = tblExp.indexPathForRow(at: buttonPosition)!
        let dictCell = arrCellType.object(at: indexPath.row) as! NSDictionary
        print(dictCell)
        let sURL = dictCell.object(forKey: "explanationImgVideoPath") as! String
        
        let vc = loadVC(strStoryboardId: SB_Question, strVCId: idVideoPlayVC) as! VideoPlayVC
        vc.strVideoURL = sURL
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func setColor(_ dictCell: NSDictionary, _ imgSelection: UIImageView, _ viewBack: UIView) {
        imgSelection.image = UIImage(named: "ans_unselected")
        viewBack.backgroundColor = UIColor.clear
        let strIsCorrectAns = dictCell.object(forKey: "isCorrectAnswer") as! String
        if strIsCorrectAns == "1" {
            imgSelection.image = UIImage(named: "ic_correct")
            viewBack.backgroundColor = Color_Hex(hex: "62DE64").withAlphaComponent(0.1)
        }
        let strAnsID = dictCell.object(forKey: "answerId") as! String
        if arrSelectedAnsID.contains(strAnsID) && strIsCorrectAns == "0" {
            imgSelection.image = UIImage(named: "ic_wrong")
            viewBack.backgroundColor = Color_Hex(hex: "DE757E").withAlphaComponent(0.1)
        }
    }
}

//MARK:- WKWEBVIEW METHOD
extension ExplanationVC: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Strat to load")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("finish to load")
        heightExplain = webView.scrollView.contentSize.height
        if heightExplain < 230 { heightExplain = 230 }
        tblExp.beginUpdates()
        tblExp.reloadData()
        tblExp.endUpdates()
    }
    
    func loadLatexString(_ webView: WKWebView, _ latexString: String) {
        webView.navigationDelegate = self
        webView.isOpaque = false
        webView.scrollView.backgroundColor = UIColor.clear
        webView.loadHTMLString(contentHtmlWithLatexString(latexString: latexString), baseURL: nil)
    }
    
    private func mathJaxScript() -> String {
        return "<script src='https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML'></script>"
    }
    
    private func configScript() -> String {
        return "<script type=\"text/x-mathjax-config\">"
            + "MathJax.Hub.Config({jax: [\"input/TeX\",\"output/HTML-CSS\"], tex2jax: {inlineMath: [['$','$']]} , \"HTML-CSS\": {linebreaks: {automatic: true}}});"
            + "</script>"
    }
    
    private func contentHtmlWithLatexString(latexString: String) -> String {
        let header = configScript() + mathJaxScript()
        let font = FONT_REGULAR(fontSize: 35)
        let text = "<html><header>\(header)</header><body>\(latexString)</body></html>"
        let finaltext = "<span style=\"font-family: \(font.fontName); font-size: \(font.pointSize); color: #21367B\">\(text)</span>"
        return finaltext
    }
}

//MARK:- SET UP DATA
extension ExplanationVC {
    func SetUpData() {
        lblTitle.text = strTitle
        
        viewSkip.isHidden = true
        viewWrong.isHidden = true
        viewRight.isHidden = true
        
        if dictQuestion["skip"] as? String ?? "0" == "1" {
            viewSkip.isHidden = false
        } else {
            let isCorrectAns = dictQuestion.object(forKey: "isCorrectAnswer") as! String
            if isCorrectAns == "1" {
                viewRight.isHidden = false
            } else {
                viewWrong.isHidden = false
            }
        }
        
        let strSelectedAnsID = dictQuestion.object(forKey: "givenanswerId") as! String
        arrSelectedAnsID = (strSelectedAnsID.components(separatedBy: ",") as NSArray).mutableCopy() as! NSMutableArray
        
        let strQuestion = dictQuestion.object(forKey: "questionText") as! String
        let strQuesImg = dictQuestion.object(forKey: "imgPath") as! String
        
        if strQuestion.count > 0 && strQuesImg.count > 0 {
            arrCellType.add(["type":QAType.QuestionTextImage.rawValue, "questionText":strQuestion, "questionImg":strQuesImg])
        }
        if strQuestion.count > 0 && strQuesImg.count <= 0 {
            arrCellType.add(["type":QAType.QuestionText.rawValue, "questionText":strQuestion])
        }
        if strQuesImg.count > 0 && strQuestion.count <= 0 {
            arrCellType.add(["type":QAType.QuestionImage.rawValue, "questionImg":strQuesImg])
        }
        
        let arrAnswer = dictQuestion.object(forKey: "answer") as! NSArray
        for k in 0..<arrAnswer.count {
            let dictAnswer = arrAnswer[k] as! NSDictionary
            let strAnsImage = dictAnswer.object(forKey: "imgPath") as! String
            let strQuestionId = dictAnswer.object(forKey: "questionId") as! String
            let strAnswerId = dictAnswer.object(forKey: "answerId") as! String
            let strAnswerText = dictAnswer.object(forKey: "answerText") as! String
            let strAnsCorrect = dictAnswer.object(forKey: "isCorrectAnswer") as! String
            
            if strAnswerText.count > 0 && strAnsImage.count > 0 {
                arrCellType.add(["type":QAType.AnswerTextImage.rawValue,"questionId":strQuestionId,"answerId":strAnswerId,"answerText":strAnswerText,"answerImg":strAnsImage,"isCorrectAnswer":strAnsCorrect])
            }
            if strAnswerText.count > 0 && strAnsImage.count <= 0 {
                arrCellType.add(["type":QAType.AnswerText.rawValue,"questionId":strQuestionId,"answerId":strAnswerId,"answerText":strAnswerText,"isCorrectAnswer":strAnsCorrect])
            }
            if strAnsImage.count > 0 && strAnswerText.count <= 0 {
                arrCellType.add(["type":QAType.AnswerImage.rawValue,"questionId":strQuestionId,"answerId":strAnswerId,"answerImg":strAnsImage,"isCorrectAnswer":strAnsCorrect])
            }
        }
        
        let strExpText = dictQuestion.object(forKey: "explanationText") as! String
        let strExpImg = dictQuestion.object(forKey: "expimgPath") as! String
        let strVideoPath = dictQuestion.object(forKey: "imgVideoPath") as? String ?? "" // need to check
        
        arrCellType.add(["type":QAType.ExplainTitle.rawValue])
        
        if strVideoPath.count > 0 {
            arrCellType.add(["type":QAType.ExplainPlayButton.rawValue, "explanationImgVideoPath":strVideoPath])
        }
        if strExpText.count > 0 && strExpImg.count > 0 {
            arrCellType.add(["type":QAType.ExplainTextImage.rawValue, "explanationText":strExpText, "explanationImg":strExpImg])
        }
        if strExpText.count > 0 && strExpImg.count <= 0 {
            arrCellType.add(["type":QAType.ExplainText.rawValue, "explanationText":strExpText])
        }
        if strExpImg.count > 0 && strExpText.count <= 0 {
            arrCellType.add(["type":QAType.ExplainImage.rawValue, "explanationImg":strExpImg])
        }
        if strVideoPath.count > 0 {
            arrCellType.add(["type":QAType.ExplainVideo.rawValue, "explanationImgVideoPath":strVideoPath])
        }
    }
}
