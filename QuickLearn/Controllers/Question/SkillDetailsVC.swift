//
//  SkillDetailsVC.swift
//  QuickLearn
//
//  Created by dhaval on 18/06/19.
//  Copyright © 2019 NILESH. All rights reserved.
//

import UIKit
import KDCircularProgress

class SkillDetailsVC: UIViewController {

    @IBOutlet weak var progressQuestion: KDCircularProgress!
    @IBOutlet weak var lblProgressPerc: UILabel!
    @IBOutlet weak var lblTotalQuestionAns: UILabel!
    @IBOutlet weak var lblTotalTime: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    var dictSubject = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(dictSubject)
        lblTitle.text = dictSubject.object(forKey: "subjectName") as? String
        apiGetSubjectResult()
    }
    
    @IBAction func btnGoHome(_ sender: Any) {
        popBack(self.navigationController, toControllerType: SWRevealViewController.self)
    }
    
    @IBAction func btnReviewAnswer(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_Question, strVCId: idSubmitedAnsVC) as! SubmitedAnsVC
        vc.dictSubject = dictSubject
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- API CALLING
extension SkillDetailsVC {
    func apiGetSubjectResult() {
        let parameter:NSDictionary = ["service":APIGetSubjectResult,
                                      "request":["data":[
                                        "p_courseId":dictSubject["courseId"] as! String,
                                        "p_subjectId":dictSubject["subjectId"] as! String]],
                                      "auth":[
                                        "id":getUserID(),
                                        "token":GetAPIToken()]]
        
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: SERVER_URL,
            service: APIGetSubjectResult,
            parameters: parameter,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "not found")
                if responseDict![kSUCCESS] as? String == "1"
                {
                    let dictData = dictionaryOfFilteredBy(dict: responseDict![kDATA] as! NSDictionary)
                    let Seconds = TimeInterval(dictData.object(forKey: "average_taken_time") as? String ?? "0") ?? 0
                    self.lblTotalTime.text = Seconds.minuteSecond
                    let totalquestion = dictData.object(forKey: "total_questions") as! String
                    let totalRightAnswer = dictData.object(forKey: "total_right_answer") as? String ?? "0"
                    self.lblTotalQuestionAns.text = String(format: "%@/%@ Correct",totalRightAnswer,totalquestion)
                    var progress = ((Double(totalRightAnswer)! * 100) / Double(totalquestion)!) / 100
                    if progress.isNaN { progress = 0 }
                    self.lblProgressPerc.text = String(format:"%d%%",Int(progress*100))
                    self.progressQuestion.progress = progress
                } else {
                    showMessage(responseDict![kMESSAGE] as! String)
                }
            }
        }
    }
}
