//
//  QuestionAnswerVC.swift
//  QuickLearn
//
//  Created by NILESH on 18/04/19.
//  Copyright © 2019 NILESH. All rights reserved.
//

import UIKit
import RichTextView

class QuestionListCell: UITableViewCell {
    @IBOutlet weak var viewQuestion: RichTextView!
}

class QuesImageCell: UITableViewCell {
    @IBOutlet weak var imgQuestion: UIImageView!
}

class QuesTextImageCell: UITableViewCell {
    @IBOutlet weak var viewQuestion: RichTextView!
    @IBOutlet weak var imgQuestion: UIImageView!
}

class AnswerListCell: UITableViewCell {
    @IBOutlet weak var viewAnswer: RichTextView!
    @IBOutlet weak var imgSelection: UIImageView!
    @IBOutlet weak var viewBack: UIView!
}

class AnsTextImageCell: UITableViewCell {
    @IBOutlet weak var viewAnswer: RichTextView!
    @IBOutlet weak var imgSelection: UIImageView!
    @IBOutlet weak var imgAnswer: UIImageView!
    @IBOutlet weak var viewBack: UIView!
}

class AnsImageCell: UITableViewCell {
    @IBOutlet weak var imgAnswer: UIImageView!
    @IBOutlet weak var imgSelection: UIImageView!
    @IBOutlet weak var viewBack: UIView!
}

protocol delegateAnsSelect {
    func SelectedQuesAns(_ sQID: String, _ sAID: String , _ CorrectAns: String)
}

protocol delegateUpdateTime {
    func UpdateTime(_ isUpdateTime: Bool)
}

class QuestionAnswerVC: UIViewController {

    @IBOutlet weak var tblQusAns: UITableView!
    
    var delegateUpdateTime: delegateUpdateTime?
    var delegateAnsSelect: delegateAnsSelect?
    
    var dictQuesAns = NSDictionary()
    var arrCellType = NSMutableArray()
    var arrSelectedAns = NSMutableArray()
    var isMultiAns: Bool = false
    var strUnselectedImg = ""
    var strSelectedImg = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        strUnselectedImg = isMultiAns ? "multi_ans_unselected" : "ans_unselected"
        strSelectedImg = isMultiAns ? "multi_ans_selected" : "ans_selected"
        StartTimerForAns()
    }
}

//MARK:- UITableViewDelegate METHOD
extension QuestionAnswerVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCellType.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dictCell = arrCellType.object(at: indexPath.row) as! NSDictionary
        let strType = dictCell.object(forKey: "type") as! String
        
        switch strType {
        case QAType.QuestionText.rawValue:
            let cell : QuestionListCell = tableView.dequeueReusableCell(withIdentifier: "QuestionListCell", for: indexPath) as! QuestionListCell
            cell.viewQuestion.update(input: dictCell.object(forKey: "questionText") as? String ?? "",
                                     latexParser: LatexParser(),
                                     font: FONT_REGULAR(fontSize: 17),
                                     textColor: Color_Hex(hex: "0F377F"),
                                     latexTextBaselineOffset: 0,
                                     completion: nil)
            return cell
        case QAType.QuestionImage.rawValue:
            let cell : QuesImageCell = tableView.dequeueReusableCell(withIdentifier: "QuesImageCell", for: indexPath) as! QuesImageCell
            cell.imgQuestion.sd_setImage(with: URL(string: dictCell.object(forKey: "questionImg") as? String ?? ""), placeholderImage: QUES_PLACE_HOLDER)
            return cell
        case QAType.QuestionTextImage.rawValue:
            let cell : QuesTextImageCell = tableView.dequeueReusableCell(withIdentifier: "QuesTextImageCell", for: indexPath) as! QuesTextImageCell
            cell.imgQuestion.sd_setImage(with: URL(string: dictCell.object(forKey: "questionImg") as? String ?? ""), placeholderImage: QUES_PLACE_HOLDER)
            cell.viewQuestion.update(input: dictCell.object(forKey: "questionText") as? String ?? "",
                                     latexParser: LatexParser(),
                                     font: FONT_REGULAR(fontSize: 17),
                                     textColor: Color_Hex(hex: "0F377F"),
                                     latexTextBaselineOffset: 0,
                                     completion: nil)
            return cell
        case QAType.AnswerText.rawValue:
            let cell : AnswerListCell = tableView.dequeueReusableCell(withIdentifier: "AnswerListCell", for: indexPath) as! AnswerListCell
            cell.viewAnswer.update(input: dictCell.object(forKey: "answerText") as? String ?? "",
                                   latexParser: LatexParser(),
                                   font: FONT_REGULAR(fontSize: 17),
                                   textColor: Color_Hex(hex: "0F377F"),
                                   latexTextBaselineOffset: 0,
                                   completion: nil)
            
            let strAnsID = dictCell.object(forKey: "answerId") as! String
            cell.imgSelection.image = UIImage(named: strUnselectedImg)
            if arrSelectedAns.contains(strAnsID) {
                cell.imgSelection.image = UIImage(named: strSelectedImg)
            }
            return cell
        case QAType.AnswerImage.rawValue:
            let cell : AnsImageCell = tableView.dequeueReusableCell(withIdentifier: "AnsImageCell", for: indexPath) as! AnsImageCell
            cell.imgAnswer.sd_setImage(with: URL(string: dictCell.object(forKey: "questionImg") as? String ?? ""), placeholderImage: QUES_PLACE_HOLDER)
            
            let strAnsID = dictCell.object(forKey: "answerId") as! String
            cell.imgSelection.image = UIImage(named: strUnselectedImg)
            if arrSelectedAns.contains(strAnsID) {
                cell.imgSelection.image = UIImage(named: strSelectedImg)
            }
            return cell
        case QAType.AnswerTextImage.rawValue:
            let cell : AnsTextImageCell = tableView.dequeueReusableCell(withIdentifier: "AnsTextImageCell", for: indexPath) as! AnsTextImageCell
            cell.viewAnswer.update(input: dictCell.object(forKey: "answerText") as? String ?? "",
                                   latexParser: LatexParser(),
                                   font: FONT_REGULAR(fontSize: 17),
                                   textColor: Color_Hex(hex: "0F377F"),
                                   latexTextBaselineOffset: 0,
                                   completion: nil)
            
            cell.imgAnswer.sd_setImage(with: URL(string: dictCell.object(forKey: "answerImg") as? String ?? ""), placeholderImage: QUES_PLACE_HOLDER)
            
            let strAnsID = dictCell.object(forKey: "answerId") as! String
            cell.imgSelection.image = UIImage(named: strUnselectedImg)
            if arrSelectedAns.contains(strAnsID) {
                cell.imgSelection.image = UIImage(named: strSelectedImg)
            }
            
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dictCell = arrCellType.object(at: indexPath.row) as! NSDictionary
        let strType = dictCell.object(forKey: "type") as! String
        switch strType {
        case QAType.AnswerText.rawValue, QAType.AnswerImage.rawValue, QAType.AnswerTextImage.rawValue:
            let strAnsID = dictCell.object(forKey: "answerId") as! String
            let strQuestionID = dictCell.object(forKey: "questionId") as! String
            var isCorrectAns = "0"
            
            if isMultiAns {
                if arrSelectedAns.contains(strAnsID) {
                    arrSelectedAns.remove(strAnsID)
                } else {
                    arrSelectedAns.add(strAnsID)
                }
                print(dictCell)
                let pred : NSPredicate = NSPredicate(format: "isCorrectAnswer = %@", "1")
                let arrTemp = arrCellType.filtered(using: pred) as NSArray
                if arrTemp.count > 0 {
                    let arrAllRightAnsID = arrTemp.value(forKeyPath: "answerId") as! NSArray
                    if arrAllRightAnsID.count == arrSelectedAns.count {
                        for i in 0..<arrAllRightAnsID.count {
                            if arrSelectedAns.contains(arrAllRightAnsID[i] as! String) {
                                isCorrectAns = "1"
                            } else {
                                isCorrectAns = "0"
                                break
                            }
                        }
                    }
                }
            } else {
                if !arrSelectedAns.contains(strAnsID) {
                    arrSelectedAns.removeAllObjects()
                    arrSelectedAns.add(strAnsID)
                }
                isCorrectAns = dictCell.object(forKey: "isCorrectAnswer") as! String
            }
            
            tblQusAns.reloadData()
            let sAnsID = arrSelectedAns.componentsJoined(by: ",")
            self.delegateAnsSelect?.SelectedQuesAns(strQuestionID, sAnsID, isCorrectAns)
            
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let dictCell = arrCellType.object(at: indexPath.row) as! NSDictionary
        let strType = dictCell.object(forKey: "type") as! String
        switch strType {
        case QAType.QuestionImage.rawValue, QAType.AnswerImage.rawValue:
            return 250
        default:
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
}

 // Find Total Time Taken for Give Answer
extension QuestionAnswerVC {
    func StartTimerForAns() {
        GlobalTakenTime = 0
        StopTimerForAns()
        GlobalTimerAnswer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
    }
    
    func StopTimerForAns() {
        GlobalTimerAnswer?.invalidate()
        GlobalTimerAnswer = nil
    }
    
    @objc func timerAction() {
        GlobalTakenTime += 1
        if APP_DELEGATE.isLearnMode {
            self.delegateUpdateTime?.UpdateTime (false)
        } else {
            self.delegateUpdateTime?.UpdateTime(true)
        }
    }
}
