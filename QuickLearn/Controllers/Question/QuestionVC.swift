//
//  QuestionVC.swift
//  QuickLearn
//
//  Created by NILESH on 18/04/19.
//  Copyright © 2019 NILESH. All rights reserved.
//

import UIKit

enum QAType: String {
    case QuestionText = "questiontext"
    case QuestionImage = "questionimage"
    case QuestionTextImage = "questiontextimage"
    case AnswerText = "answertext"
    case AnswerImage = "answerimage"
    case AnswerTextImage = "answertextimage"
    case ExplainTitle = "explanationtitle"
    case ExplainText = "explaintext"
    case ExplainImage = "explainimage"
    case ExplainTextImage = "explaintextimage"
    case ExplainVideo = "explainvideo"
    case ExplainPlayButton = "explanationplaybutton"
}

class QuestionVC: UIViewController {

    @IBOutlet var viewQusAns: UIView!
    @IBOutlet var lblSubject: UILabel!
    @IBOutlet var lblTotalItem: UILabel!
    @IBOutlet var lblOutOfItem: UILabel!
    @IBOutlet var progressBar: UIProgressView!
    @IBOutlet var btnDone: UIButton!
    @IBOutlet var imgDone: UIImageView!
    @IBOutlet var viewSkip: UIView!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var viewPrevious: UIView!
    @IBOutlet var widthPreviousView: NSLayoutConstraint!
    
    var pageMenu : CAPSPageMenu?
    var arrQuesAns = NSMutableArray()
    var dictSubject = NSDictionary()
    var TotalQuestion: Int = 0
    var attemptQues: Int = 0
    var sQuestionID: String = "0"
    var sAnswerID: String = "0"
    var isCorrectAns: String = "0"
    var isStartOver: Bool = false
    var isLastQuestion: Bool = false
    
    var TotalExamTime: Double = 0
    var ExamTimer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblSubject.text = dictSubject.object(forKey: "subjectName") as? String
        TotalQuestion = Int(dictSubject.object(forKey: "totalquestion") as? String ?? "0") ?? 0
        lblTotalItem.text = "\(TotalQuestion) Item"
        progressBar.progress = 0
        lblOutOfItem.text = ""
        lblTime.text = ""
        SetButonImage()
        apiGetQuestionAnswer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        StopTimer()
    }
    
    func SetButonImage() {
        if APP_DELEGATE.isLearnMode {
            widthPreviousView.constant = 90
            viewPrevious.alpha = 1
        } else {
            widthPreviousView.constant = 0
            viewPrevious.alpha = 0
        }
        if isLastQuestion || arrQuesAns.count == 1 {
            imgDone.image = UIImage(named: "btn_finish")
            return
        }
        
        if APP_DELEGATE.isLearnMode {
            imgDone.image = UIImage(named: "btn_done")
        } else {
            imgDone.image = UIImage(named: "btn_next")
        }
    }
    
    //MARK:- UIBUTTON ACTIONS
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnChangeMode(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_Alerts, strVCId: idSelectModeVC) as! SelectModeVC
        vc.modalPresentationStyle = .overCurrentContext
        vc.dictSubject = dictSubject
        vc.delegateMode = self
        UIView.transition(with:APP_DELEGATE.window!, duration:0.3, options:.transitionCrossDissolve, animations: {
            self.present(vc, animated: false, completion: nil)
        }, completion:nil)
    }
    
    @IBAction func btnSkip(_ sender: Any) {
        StopTimer()
        let dictQues = arrQuesAns.object(at: pageMenu!.currentPageIndex) as! NSDictionary
        sQuestionID = dictQues.object(forKey: "questionId") as! String
        apiAttemptQues("1")
        AfterSaveAnswer(NSDictionary(), true)
    }
    
    @IBAction func btnDone(_ sender: Any) {
        StopTimer()
        apiAttemptQues("0")
        apiSaveAnswer()
    }
    
    @IBAction func btnPrevious(_ sender: Any) {
        //pageMenu?.moveToPage(pageMenu!.currentPageIndex-1)
        let vc = loadVC(strStoryboardId: SB_Question, strVCId: idSubmitedAnsVC) as! SubmitedAnsVC
        vc.dictSubject = dictSubject
        vc.isNormalBack = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- LOCAL CLASS METHODS
    func StopTimer() {
        GlobalTimerAnswer?.invalidate()
        GlobalTimerAnswer = nil
    }
}

//MARK:- SET APP MODE
extension QuestionVC: delegateMode {
    func SetSelectedMode(_ type: Int, object: NSDictionary) {
        
        /*
        type (0, 1)
        0 > Learning Mode
        1 > Skill Mode
        */
        
        if (type == 0 && APP_DELEGATE.isLearnMode) || (type == 1 && !APP_DELEGATE.isLearnMode)  { return }
        
        runAfterTime(time: 0.5) {
            let msg = APP_DELEGATE.isLearnMode ? "Are you sure exit from Learning Mode?" : "Are you sure exit from Skill Test mode?"
            let alert = UIAlertController(title: nil, message: msg, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Resume", style: .default, handler: { (alertAction) in
            }))
            alert.addAction(UIAlertAction(title: "Save for later", style: .default, handler: { (alertAction) in
                if APP_DELEGATE.isLearnMode {
                    //self.RestoreAllQuestions(type, object)
                    self.ChangeMode(type, object)
                    self.StartExamTimer(true)
                } else {
                    //skill mode to learning mode
                    //Save all the answer options and remaining timer value.
                    let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Start Over", style: .default, handler: { (alertAction) in
                        self.RestoreAllQuestions(type, object)
                    }))
                    alert.addAction(UIAlertAction(title: "Continue from Previous", style: .default, handler: { (alertAction) in
                        self.StopExamTimer()
                        self.ChangeMode(type, object)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                }
            }))
            alert.addAction(UIAlertAction(title: "Exit", style: .default, handler: { (alertAction) in
                self.RestoreAllQuestions(type, object)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func RestoreAllQuestions(_ type: Int, _ object: NSDictionary) {
        ChangeMode(type, object)
        self.apiRestartCourse()
    }
    
    func ChangeMode(_ type: Int, _ object: NSDictionary) {
        let sSubject = object["subjectId"] as! String
        SaveSelectedMode(type, sSubject)
        SetSelectedModeFlag(sSubject)
        self.lblTime.text = ""
        self.SetButonImage()
    }
}

//MARK:- ANSWER SELECTION DELEGATE
extension QuestionVC: delegateAnsSelect {
    func SelectedQuesAns(_ sQID: String, _ sAID: String , _ CorrectAns: String) {
        sAnswerID = sAID
        sQuestionID = sQID
        isCorrectAns = CorrectAns
        
        btnDone.isEnabled = true
        imgDone.alpha = 1
    }
}

//MARK:- SET PAGEMENU
extension QuestionVC: CAPSPageMenuDelegate {
    func SetPageMenu() {
        pageMenu?.view.removeFromSuperview()
        pageMenu?.removeFromParentViewController()
        
        SetSelectedModeFlag(dictSubject["subjectId"] as! String)
        
        var arrController : [UIViewController] = []
        for i in 0..<arrQuesAns.count {
            let vc = loadVC(strStoryboardId: SB_Question, strVCId: idQuestionAnswerVC) as! QuestionAnswerVC
            vc.delegateAnsSelect = self
            
            let arrCell = NSMutableArray()
            let dictQues = arrQuesAns.object(at: i) as! NSDictionary
            
            //let dictExplain = dictQues.object(forKey: "explanation") as! NSDictionary
            //let strQuestion = dictExplain.object(forKey: "explanationText") as! String
  
            let strQuestion = (dictQues.object(forKey: "questionText") as! String).replacingOccurrences(of: "$$", with: "$")  //nileshlatex
            let strQuesImg = dictQues.object(forKey: "imgPath") as! String
            
            if strQuestion.count > 0 && strQuesImg.count > 0 {
                arrCell.add(["type":QAType.QuestionTextImage.rawValue, "questionText":strQuestion, "questionImg":strQuesImg])
            }
            if strQuestion.count > 0 && strQuesImg.count <= 0 {
                arrCell.add(["type":QAType.QuestionText.rawValue, "questionText":strQuestion])
            }
            if strQuesImg.count > 0 && strQuestion.count <= 0 {
                arrCell.add(["type":QAType.QuestionImage.rawValue, "questionImg":strQuesImg])
            }

            let arrAnswer = dictQues.object(forKey: "answer") as! NSArray
            for k in 0..<arrAnswer.count {
                let dictAnswer = arrAnswer[k] as! NSDictionary
                let strAnsImage = dictAnswer.object(forKey: "imgPath") as! String
                let strQuestionId = dictAnswer.object(forKey: "questionId") as! String
                let strAnswerId = dictAnswer.object(forKey: "answerId") as! String
                let strAnswerText = dictAnswer.object(forKey: "answerText") as! String
                let strAnsCorrect = dictAnswer.object(forKey: "isCorrectAnswer") as! String
                
                if strAnswerText.count > 0 && strAnsImage.count > 0 {
                    arrCell.add(["type":QAType.AnswerTextImage.rawValue, "questionId":strQuestionId, "answerId":strAnswerId, "answerText":strAnswerText, "answerImg":strAnsImage, "isCorrectAnswer":strAnsCorrect])
                }
                if strAnswerText.count > 0 && strAnsImage.count <= 0 {
                    arrCell.add(["type":QAType.AnswerText.rawValue,"questionId":strQuestionId,"answerId":strAnswerId,"answerText":strAnswerText,"isCorrectAnswer":strAnsCorrect])
                }
                if strAnsImage.count > 0 && strAnswerText.count <= 0 {
                    arrCell.add(["type":QAType.AnswerImage.rawValue,"questionId":strQuestionId,"answerId":strAnswerId,"answerImg":strAnsImage,"isCorrectAnswer":strAnsCorrect])
                }
            }
        
            let pred : NSPredicate = NSPredicate(format: "isCorrectAnswer = %@", "1")
            let arrTemp = arrAnswer.filtered(using: pred) as NSArray
            if arrTemp.count > 1 {
                vc.isMultiAns = true
            }
            vc.delegateUpdateTime = self
            vc.dictQuesAns = dictQues
            vc.arrCellType = arrCell
            arrController.append(vc)
        }
        
        let parameters: [CAPSPageMenuOption] = [ .hideTopMenuBar(true) ]
        let frame = CGRect(x: 0, y: 0, width: self.viewQusAns.frame.size.width, height: self.viewQusAns.frame.size.height)
        pageMenu = CAPSPageMenu(viewControllers: arrController, frame: frame, pageMenuOptions: parameters)
        pageMenu?.controllerScrollView.isScrollEnabled = false
        pageMenu?.view.backgroundColor = UIColor.clear
        pageMenu?.delegate = self
        self.addChildViewController(pageMenu!)
        self.viewQusAns.addSubview(pageMenu!.view)
        pageMenu!.didMove(toParentViewController: self)
        
        StartExamTimer(false)
    }
    
    func willMoveToPage(_ controller: UIViewController, index: Int) {
        let currentQues = index+1+attemptQues
        lblOutOfItem.text =  "\(currentQues) of \(TotalQuestion)"
        isLastQuestion = false
        if currentQues == TotalQuestion {
            isLastQuestion = true
        }
        btnDone.isEnabled = false
        imgDone.alpha = 0.5
    }
}

//MARK: DELEGATE METHOD
extension QuestionVC: delegateExplain, delegateUpdateTime {
    func UpdateTime(_ isUpdateTime: Bool) {
        if isUpdateTime {
            /*
            let (h, m, s) = secondsToHoursMinutesSeconds(seconds: Int(GlobalTakenTime))
            if h <= 0 {
                lblTime.text = String(format:"%02d:%02d",m,s)
            } else {
                lblTime.text = String(format:"%02d:%02d:%02d",h,m,s)
            }*/
        }
        SetButonImage()
    }
    
    func GoForNextQuestion() {
        pageMenu?.moveToPage(pageMenu!.currentPageIndex+1)
        if progressBar.progress >= 1 {
            GoForQuesList()
        }
    }
    
    func GoForQuesList() {
        if APP_DELEGATE.isLearnMode {
            let vc = loadVC(strStoryboardId: SB_Question, strVCId: idSubmitedAnsVC) as! SubmitedAnsVC
            vc.dictSubject = dictSubject
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            GoToSkillDetails()
        }
    }
    
    func GoToSkillDetails() {
        let vc = loadVC(strStoryboardId: SB_Question, strVCId: idSkillDetailsVC) as! SkillDetailsVC
        vc.dictSubject = self.dictSubject
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- API CALLING
extension QuestionVC {
    func apiGetQuestionAnswer() {
        APP_DELEGATE.arrQusAnsSelected = NSMutableArray()
        let parameter:NSDictionary = ["service":APIQuesAns,
                                      "request":["data":[
                                        "course_id":dictSubject["courseId"] as! String,
                                        "subject_id":dictSubject["subjectId"] as! String]],
                                      "auth":[
                                        "id":getUserID(),
                                        "token":GetAPIToken()]]
        
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: SERVER_URL,
            service: APIQuesAns,
            parameters: parameter,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "not found")
                if responseDict![kSUCCESS] as? String == "1"
                {
                    self.arrQuesAns = arrayOfFilteredBy(arr: responseDict?.object(forKey: kDATA) as! NSArray).mutableCopy() as! NSMutableArray
                    print(self.arrQuesAns)
                    
                    if self.arrQuesAns.count > 0 {
                        self.attemptQues = self.TotalQuestion - self.arrQuesAns.count
                        self.lblOutOfItem.text = "\(self.attemptQues+1) of \(self.TotalQuestion)"
                        self.progressBar.setProgress(Float(self.attemptQues * 100 / self.TotalQuestion) / 100, animated: true)
                        self.SetPageMenu()
                    } else {
                        /*
                        showMessage("No Question.")
                        self.navigationController?.popViewController(animated: true)*/
                        self.GoToSkillDetails()
                    }
                } else {
                    /*
                    showMessage(responseDict![kMESSAGE] as! String)
                    self.navigationController?.popViewController(animated: true)*/
                    self.GoToSkillDetails()
                }
            }
        }
    }
    
    func apiSaveAnswer() {
        let parameter:NSDictionary = ["service":APISaveAnswer,
                                      "request":["data":[
                                        "p_questionId":sQuestionID,
                                        "p_answerId":sAnswerID,
                                        "p_isCorrectAnswer":isCorrectAns,
                                        "p_stateId":"0",
                                        "p_courseId":dictSubject.object(forKey: "courseId") as! String,
                                        "p_subjectId":dictSubject.object(forKey: "subjectId") as! String,
                                        "p_timetaken":"\(GlobalTakenTime)"]],
                                      "auth":[
                                        "id":getUserID(),
                                        "token":GetAPIToken()]]
        
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: SERVER_URL,
            service: APISaveAnswer,
            parameters: parameter,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "not found")
                if responseDict![kSUCCESS] as? String == "1"
                {
                    self.AfterSaveAnswer(responseDict!,false)
                } else {
                    showMessage(responseDict![kMESSAGE] as! String)
                }
            }
        }
    }
    
    func apiAttemptQues(_ isSkip: String) {
        let parameter:NSDictionary = ["service":APIAttempt,
                                      "request":["data": [
                                        "p_questionId":sQuestionID,
                                        "p_skip":isSkip]],
                                      "auth":[
                                        "id":getUserID(),
                                        "token":GetAPIToken()]]
        
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: SERVER_URL,
            service: APIAttempt,
            parameters: parameter,
            method: .post,
            showLoader: false)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "not found")
                if responseDict![kSUCCESS] as? String == "1"
                {
                }
            }
        }
    }
    
    func apiRestartCourse() {
        let parameter:NSDictionary = ["service":APIRestartCourse,
                                      "request":["data":[
                                        "course_id":dictSubject["courseId"] as! String,
                                        "subject_id":dictSubject["subjectId"] as! String]],
                                      "auth":[
                                        "id":getUserID(),
                                        "token":GetAPIToken()]]
        
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: SERVER_URL,
            service: APIQuesAns,
            parameters: parameter,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "not found")
                if responseDict![kSUCCESS] as? String == "1"
                {
                    self.apiGetQuestionAnswer()
                }
                else
                {
                    showMessage(responseDict![kMESSAGE] as! String)
                }
            }
        }
    }
}

//MARK:- Other Methods
extension QuestionVC {
    func AfterSaveAnswer(_ responseDict: NSDictionary, _ isSkip: Bool) {
        if APP_DELEGATE.arrQusAnsSelected.contains(self.sQuestionID) {
            APP_DELEGATE.arrQusAnsSelected.remove(self.sQuestionID)
        }
        APP_DELEGATE.arrQusAnsSelected.add(self.sQuestionID)
        let SubmittedAns = APP_DELEGATE.arrQusAnsSelected.count + self.attemptQues
        self.progressBar.setProgress(Float(SubmittedAns * 100 / self.TotalQuestion) / 100, animated: true)
        if isSkip {
            pageMenu?.moveToPage(pageMenu!.currentPageIndex+1)
            if SubmittedAns >= self.TotalQuestion {
                self.GoForQuesList()
            }
        } else {
            if APP_DELEGATE.isLearnMode {
                let vc = loadVC(strStoryboardId: SB_Question, strVCId: idExplanationVC) as! ExplanationVC
                vc.dictQuestion = responseDict[kDATA] as! NSDictionary
                vc.strTitle = self.dictSubject.object(forKey: "subjectName") as! String
                vc.delegateExplain = self
                vc.isDirectFromQues = true
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                pageMenu?.moveToPage(pageMenu!.currentPageIndex+1)
                if SubmittedAns >= self.TotalQuestion {
                    self.GoForQuesList()
                }
            }
        }
    }
}

// Find Total Time Taken for Give Answer
extension QuestionVC {
    func StartExamTimer(_ isChangeMode: Bool) {
        if APP_DELEGATE.isLearnMode {
            TotalExamTime = 0
            StopExamTimer()
        } else {
            if isChangeMode {
                TotalExamTime = Double(self.arrQuesAns.count-pageMenu!.currentPageIndex) * 60
            } else {
                TotalExamTime = Double(self.arrQuesAns.count) * 60
            }
            
            StopExamTimer()
            ExamTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
        }
    }
    
    func StopExamTimer() {
        ExamTimer?.invalidate()
        ExamTimer = nil
        lblTime.text = ""
    }
    
    @objc func timerAction() {
        TotalExamTime -= 1
        let (h, m, s) = secondsToHoursMinutesSeconds(seconds: Int(TotalExamTime))
        if h <= 0 {
            lblTime.text = String(format:"%02d:%02d",m,s)
        } else {
            lblTime.text = String(format:"%02d:%02d:%02d",h,m,s)
        }
        
        if TotalExamTime <= 0 {
            // time over for exam
            StopExamTimer()
            GoToSkillDetails()
        }
    }
}
