//
//  VideoPlayVC.swift
//  QuickLearn
//
//  Created by dhaval on 11/10/19.
//  Copyright © 2019 NILESH. All rights reserved.
//

import UIKit
import WebKit

class VideoPlayVC: UIViewController {

    @IBOutlet var webView: WKWebView!
    @IBOutlet var actiity: UIActivityIndicatorView!
    
    var strVideoURL: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.navigationDelegate = self
        webView.isOpaque = false
        webView.scrollView.backgroundColor = UIColor.clear
        if let videoURL:URL = URL(string: strVideoURL) {
             let request:URLRequest = URLRequest(url: videoURL)
             webView.load(request)
        }
    }

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK:- WKWEBVIEW METHOD
extension VideoPlayVC: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Start to load")
        actiity.startAnimating()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("finish to load")
        actiity.stopAnimating()
    }
}
