//
//  TabBarVC.swift
//  QuickLearn
//
//  Created by NILESH on 18/04/19.
//  Copyright © 2019 NILESH. All rights reserved.
//

import UIKit

class TabBarVC: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        print("Selected item")
    }
    
    //MARK:- UITabBar Delegate Method
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if !isUserLogin() {
            if self.selectedIndex == 1 || self.selectedIndex == 2 {
                self.selectedIndex = 0
                let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: idLoginVC) as! LoginVC
                vc.isShowBack = true
                UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                    self.navigationController?.pushViewController(vc, animated: false)
                }, completion: nil)
            }
        }
    }
}
