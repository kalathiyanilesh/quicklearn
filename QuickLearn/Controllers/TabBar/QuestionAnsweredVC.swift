//
//  QuestionAnsweredVC.swift
//  QuickLearn
//
//  Created by NILESH on 22/04/19.
//  Copyright © 2019 NILESH. All rights reserved.
//

import UIKit
import KDCircularProgress

class AnsweredCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTotalItem: UILabel!
    @IBOutlet weak var progressbar: UIProgressView!
}

class QuestionAnsweredVC: UIViewController {

    @IBOutlet weak var tblProgress: UITableView!
    @IBOutlet weak var progressQuestion: KDCircularProgress!
    @IBOutlet weak var lblProgressPerc: UILabel!
    @IBOutlet weak var lblTotalQuestionAns: UILabel!
    @IBOutlet weak var lblAvgTime: UILabel!
    @IBOutlet weak var lblMostAns: UILabel!
    
    var arrSubject = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !isUserLogin() { return }
        apiGetProgress()
    }
}

//MARK:- UITableViewDelegate METHOD
extension QuestionAnsweredVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSubject.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : AnsweredCell = tableView.dequeueReusableCell(withIdentifier: "AnsweredCell", for: indexPath) as! AnsweredCell
        let dictSubject = arrSubject[indexPath.row] as! NSDictionary
        cell.lblTitle.text = dictSubject.object(forKey: "courseName") as? String
        cell.lblTotalItem.text = "\(dictSubject.object(forKey: "totalquestion") as! String) Item"
        
        let totalquestion = dictSubject.object(forKey: "totalquestion") as! String
        if Int(totalquestion)! == 0 {
            cell.progressbar.progress = 0
        } else {
            let totalattempt = dictSubject.object(forKey: "totalattempt") as? String ?? "0"
            cell.progressbar.setProgress(Float(Int(totalattempt)! * 100 / Int(totalquestion)!) / 100, animated: true)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = loadVC(strStoryboardId: SB_Question, strVCId: idSubjectListVC) as! SubjectListVC
        vc.dictCourse = arrSubject[indexPath.row] as! NSDictionary
        vc.isShowProgress = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
}

//MARK:- API CALLING
extension QuestionAnsweredVC {
    func apiGetProgress() {
        let parameter:NSDictionary = ["service":APIGetProgress,
                                      "request":[],
                                      "auth":[
                                        "id":getUserID(),
                                        "token":GetAPIToken()]]
        
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: SERVER_URL,
            service: APIGetProgress,
            parameters: parameter,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "not found")
                if responseDict![kSUCCESS] as? String == "1"
                {
                    let dictAllOver = responseDict?.object(forKey: "allover") as! NSDictionary
                    let rightAns = dictAllOver.object(forKey: "rightans") as! String
                    let totalAttempt = dictAllOver.object(forKey: "totalattempt") as! String
                    let Seconds = TimeInterval(dictAllOver.object(forKey: "avgtime") as? String ?? "0") ?? 0
                    self.lblAvgTime.text = Seconds.minuteSecond
                    self.lblTotalQuestionAns.text = String(format:"%@/%@ Correct",rightAns, totalAttempt)
                    var progress = ((Double(rightAns)! * 100) / Double(totalAttempt)!) / 100
                    if progress.isNaN { progress = 0 }
                    self.lblProgressPerc.text = String(format:"%d%%",Int(progress*100))
                    self.progressQuestion.progress = progress
                    self.arrSubject = (responseDict?.object(forKey: kDATA) as! NSArray).mutableCopy() as! NSMutableArray
                    self.tblProgress.reloadData()
                } else {
                    showMessage(responseDict![kMESSAGE] as! String)
                }
            }
        }
    }
}

