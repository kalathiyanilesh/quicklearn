//
//  HomeVC.swift
//  QuickLearn
//
//  Created by NILESH on 09/04/19.
//  Copyright © 2019 NILESH. All rights reserved.
//

import UIKit

class QuestionCell: UITableViewCell {
    @IBOutlet weak var btnSeeAll: UIButton!
    @IBOutlet weak var CollQuestion: UICollectionView!
    @IBOutlet weak var lblCourseName: UILabel!
    @IBOutlet weak var imgLock: UIImageView!
}

extension QuestionCell {
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        CollQuestion.delegate = dataSourceDelegate
        CollQuestion.dataSource = dataSourceDelegate
        CollQuestion.tag = row
        CollQuestion.setContentOffset(CollQuestion.contentOffset, animated:false)
        CollQuestion.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { CollQuestion.contentOffset.x = newValue }
        get { return CollQuestion.contentOffset.x }
    }
}

class CollQuestionCell: UICollectionViewCell {
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var lblFreeQuestion: UILabel!
}

class HomeVC: UIViewController {

    @IBOutlet weak var tblCourse: UITableView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var btnMenu: UIButton!
    
    var storedOffsets = [Int: CGFloat]()
    var arrCourse = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        btnMenu.isHidden = !isUserLogin()
        if !isUserLogin() && arrCourse.count > 0 { return }
        apiGetCourse()
    }
    
    @IBAction func btnMenu(_ sender: Any) {
        self.revealViewController().revealToggle(animated:true)
    }
}

//MARK:- UITableViewDelegate METHOD
extension HomeVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCourse.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : QuestionCell = tableView.dequeueReusableCell(withIdentifier: "QuestionCell", for: indexPath) as! QuestionCell
        let dictCourse = arrCourse[indexPath.row] as! NSDictionary
        cell.imgLock.isHidden = false
        if dictCourse.object(forKey: "is_purchase") as! String == "1" {
            cell.imgLock.isHidden = true
        }
        cell.lblCourseName.text = dictCourse["courseName"] as? String
        cell.btnSeeAll.addTarget(self, action: #selector(btnSeeAll(sender:)), for: .touchUpInside)
        cell.btnSeeAll.isHidden = !isUserLogin()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let questionCell = cell as? QuestionCell else { return }
        questionCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
        questionCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let questionCell = cell as? QuestionCell else { return }
        storedOffsets[indexPath.row] = questionCell.collectionViewOffset
    }
    
    @objc func btnSeeAll(sender: UIButton!) {
        let buttonPosition = sender.convert(CGPoint.zero, to: tblCourse)
        let indexPath = tblCourse.indexPathForRow(at: buttonPosition)
        let vc = loadVC(strStoryboardId: SB_Question, strVCId: idSubjectListVC) as! SubjectListVC
        vc.dictCourse = arrCourse[indexPath!.row] as! NSDictionary
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- UICollectionViewDelegate METHOD
extension HomeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let dictCourse = arrCourse[collectionView.tag] as! NSDictionary
        let arrSubject = dictCourse.object(forKey: "subject") as! NSArray
        return arrSubject.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CollQuestionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollQuestionCell", for: indexPath) as! CollQuestionCell
        let dictCourse = arrCourse[collectionView.tag] as! NSDictionary
        let arrSubject = dictCourse.object(forKey: "subject") as! NSArray
        let dictSubject = arrSubject[indexPath.row] as! NSDictionary
        cell.lblSubject.text = dictSubject["subjectName"] as? String
        cell.lblFreeQuestion.text = "\(dictSubject.object(forKey: "totalquestion") as! String) Questions"
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
        if !isConnectedToNetwork() { return }
        
        if !isUserLogin() {
            let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: idLoginVC) as! LoginVC
            vc.isShowBack = true
            UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                self.navigationController?.pushViewController(vc, animated: false)
            }, completion: nil)
            
            return
        }
        
        let dictCourse = arrCourse[collectionView.tag] as! NSDictionary
        if dictCourse.object(forKey: "is_purchase") as! String == "1" {
            let dictCourse = arrCourse[collectionView.tag] as! NSDictionary
            let arrSubject = dictCourse.object(forKey: "subject") as! NSArray
            let dictSubject = arrSubject[indexPath.row] as! NSDictionary
            
            let totalQuestion = Int(dictSubject.object(forKey: "totalquestion") as! String)!
            if totalQuestion <= 0 { return }
            
            let totalAttempt = Int(dictSubject.object(forKey: "totalattempt") as! String)!
            if  totalQuestion == totalAttempt {
                askForStartOver(dictSubject, true)
            } else {
                let sSubjectID = dictSubject["subjectId"] as! String
                if isModeSelected(sSubjectID) {
                    if totalAttempt > 0 {
                        askForStartOver(dictSubject,false)
                    } else {
                        GoForQuestion(dictSubject)
                    }
                } else {
                    let vc = loadVC(strStoryboardId: SB_Alerts, strVCId: idSelectModeVC) as! SelectModeVC
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.dictSubject = dictSubject
                    vc.delegateMode = self
                    UIView.transition(with:APP_DELEGATE.window!, duration:0.3, options:.transitionCrossDissolve, animations: {
                        self.present(vc, animated: false, completion: nil)
                    }, completion:nil)
                }
            }
        } else {
            let vc = loadVC(strStoryboardId: SB_TABBAR, strVCId: idPaymentInfoVC) as! PaymentInfoVC
            vc.dictCourse = dictCourse
            UIView.transition(with:APP_DELEGATE.window!, duration:0.3, options:.transitionCrossDissolve, animations: {
                self.navigationController?.pushViewController(vc, animated: false)
            }, completion:nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:130, height:110)
    }
}

//MARK:- SET APP MODE
extension HomeVC: delegateMode {
    func SetSelectedMode(_ type: Int, object: NSDictionary) {
        SaveSelectedMode(type, object.object(forKey: "subjectId") as! String)
        let totalQuestion = Int(object.object(forKey: "totalquestion") as! String)!
        let totalAttempt = Int(object.object(forKey: "totalattempt") as! String)!
        if  totalQuestion == totalAttempt {
            askForStartOver(object, true)
        } else if totalAttempt > 0 {
            askForStartOver(object, false)
        } else {
            GoForQuestion(object)
        }
    }
    
    func GoForQuestion(_ object: NSDictionary) {
        let vc = loadVC(strStoryboardId: SB_Question, strVCId: idQuestionVC) as! QuestionVC
        vc.dictSubject = object
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func askForStartOver(_ dictSubject: NSDictionary, _ isGoSkillDetails: Bool) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Start Over", style: .default, handler: { (alertAction) in
            self.apiRestartCourse(dictSubject)
        }))
        alert.addAction(UIAlertAction(title: "Continue from previous", style: .default, handler: { (alertAction) in
            if isGoSkillDetails {
                let vc = loadVC(strStoryboardId: SB_Question, strVCId: idSkillDetailsVC) as! SkillDetailsVC
                vc.dictSubject = dictSubject
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                self.GoForQuestion(dictSubject)
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (alertAction) in
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

//MARK:- API CALLING
extension HomeVC {
    func apiGetCourse() {
        if !isConnectedToNetwork() && (UserDefaults.standard.object(forKey: "arraycourse") != nil){
            self.arrCourse = UserDefaults.standard.object(forKey: "arraycourse") as! NSMutableArray
            self.tblCourse.reloadData()
            return
        }
        activity.startAnimating()
        let parameter:NSDictionary = ["service":APIGetCourse,
                                      "request":[],
                                      "auth":[
                                        "id":getUserID(),
                                        "token":GetAPIToken()]]
        
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: SERVER_URL,
            service: APIGetCourse,
            parameters: parameter,
            method: .post,
            showLoader: false)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "not found")
                if responseDict![kSUCCESS] as? String == "1"
                {
                    self.arrCourse = arrayOfFilteredBy(arr: responseDict?.object(forKey: kDATA) as! NSArray).mutableCopy() as! NSMutableArray
                    UserDefaults.standard.set(self.arrCourse, forKey: "arraycourse")
                    UserDefaults.standard.synchronize()
                    self.tblCourse.reloadData()
                } else {
                    showMessage(responseDict![kMESSAGE] as! String)
                }
            }
            
            self.activity.stopAnimating()
        }
    }
    
    func apiRestartCourse(_ dictSubject: NSDictionary) {
        let parameter:NSDictionary = ["service":APIRestartCourse,
                                      "request":["data":[
                                        "course_id":dictSubject["courseId"] as! String,
                                        "subject_id":dictSubject["subjectId"] as! String]],
                                      "auth":[
                                        "id":getUserID(),
                                        "token":GetAPIToken()]]
        
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: SERVER_URL,
            service: APIQuesAns,
            parameters: parameter,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "not found")
                if responseDict![kSUCCESS] as? String == "1"
                {
                    self.GoForQuestion(dictSubject)
                }
                else
                {
                    showMessage(responseDict![kMESSAGE] as! String)
                }
            }
        }
        
    }
}
