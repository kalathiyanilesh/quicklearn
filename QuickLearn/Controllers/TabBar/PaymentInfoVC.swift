//
//  PaymentInfoVC.swift
//  QuickLearn
//
//  Created by dhaval on 12/08/19.
//  Copyright © 2019 NILESH. All rights reserved.
//

import UIKit

class PaymentInfoVC: UIViewController {

    @IBOutlet weak var lblCourseName: UILabel!
    var dictCourse = NSDictionary()
    
    override func viewDidLoad() {
        print(dictCourse)
        lblCourseName.text = dictCourse.object(forKey: "courseName") as? String
        super.viewDidLoad()
    }
    
    @IBAction func btnPayment(_ sender: UIButton) {
        let vc = loadVC(strStoryboardId: SB_TABBAR, strVCId: idPaymentVC) as! PaymentVC
        vc.month = sender.tag
        vc.dictCourse = dictCourse
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnClose(_ sender: Any) {
        UIView.transition(with:APP_DELEGATE.window!, duration:0.3, options:.transitionCrossDissolve, animations: {
            self.navigationController?.popViewController(animated: false)
        }, completion:nil)
    }
    
}
