//
//  PaymentVC.swift
//  QuickLearn
//
//  Created by dhaval on 07/08/19.
//  Copyright © 2019 NILESH. All rights reserved.
//

import UIKit
import WebKit

class PaymentVC: UIViewController {

    var month: Int = 0
    var sPaymentID = ""
    var dictCourse = NSDictionary()
    
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        apiMakePayment()
    }
    
    @IBAction func btnBack(_ sender: Any) {
        apiCheckPaymentStatus()
    }
}

//MARK:- WKWEBVIEW METHOD
extension PaymentVC: WKNavigationDelegate {
    func LoadWebView(_ sURL: String) {
        webView.navigationDelegate = self
        webView.isOpaque = false
        webView.scrollView.backgroundColor = UIColor.clear
        let url = NSURL(string: sURL)
        let request = NSURLRequest(url: url! as URL)
        webView.load(request as URLRequest)
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
        activity.stopAnimating()
        showMessage(error.localizedDescription)
        self.navigationController?.popViewController(animated: true)
    }

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Strat to load")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("finish to load")
        activity.stopAnimating()
    }
}

extension PaymentVC {
    func apiMakePayment() {
        var totalRs = "10"
        if month == 1 {
            totalRs = "10"
        } else if month == 3 {
            totalRs = "25"
        } else if month == 12 {
            totalRs = "100"
        }
        let parameter:NSDictionary = ["service":APIPayment,
                                      "request":["data":[
                                        "p_courseId":dictCourse.object(forKey: "courseId") as? String ?? "",
                                        "p_subjectId":"0",
                                        "p_amount":totalRs,
                                        "p_month":"\(month)"]],
                                      "auth":[
                                        "id":getUserID(),
                                        "token":GetAPIToken()]]
        
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: SERVER_URL,
            service: APIPayment,
            parameters: parameter,
            method: .post,
            showLoader: false)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "not found")
                if responseDict![kSUCCESS] as? String == "1"
                {
                    let dictData: NSDictionary = dictionaryOfFilteredBy(dict: responseDict?.object(forKey: kDATA) as! NSDictionary)
                    self.sPaymentID = dictData["id"] as! String
                    let longurl = dictData["longurl"] as! String
                    self.LoadWebView(longurl)
                } else {
                    showMessage(responseDict![kMESSAGE] as! String)
                }
            }
        }
    }
    
    func apiCheckPaymentStatus() {
        let parameter:NSDictionary = ["service":APIPaymentStatus,
                                      "request":["data":["id":sPaymentID]],
                                      "auth":[
                                        "id":getUserID(),
                                        "token":GetAPIToken()]]
        
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: SERVER_URL,
            service: APIPaymentStatus,
            parameters: parameter,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "not found")
                if responseDict![kSUCCESS] as? String == "1"
                {
                    let vc = loadVC(strStoryboardId: SB_TABBAR, strVCId: idNavHome)
                    UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                        APP_DELEGATE.window?.rootViewController = vc
                    }, completion: nil)
                } else {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
}
