//
//  UserVC.swift
//  QuickLearn
//
//  Created by NILESH on 09/04/19.
//  Copyright © 2019 NILESH. All rights reserved.
//

import UIKit

enum CellType: String {
    case EDITPROFILE = "editprofile"
    case ADDGOAL = "addgoal"
    case SUBSCRIBE = "subscribenow"
    case CHANGEPASSWORD = "changepassword"
    case SHAREAPP = "shareapp"
}

class ProfileCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnProfiles: UIButton!
}

class UserVC: UIViewController {

    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var tblUser: UITableView!
    @IBOutlet weak var lblUser: UILabel!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    var arrUser = NSMutableArray()
    var arrSubscribed = NSArray()
    var dictUser = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !isUserLogin() { return }
        dictUser = getUserInfo()
        lblUser.text = String(format:"%@ %@",dictUser.object(forKey: "clientFirstName") as! String,dictUser.object(forKey: "clientLastName") as! String)
        imgUser.sd_setImage(with: URL(string: dictUser.object(forKey: "clientProfile") as? String ?? ""), placeholderImage: UIImage(named: "default_pic"))
        
        apiGetSubscription()
    }
}

//MARK:- UITableViewDelegate METHOD
extension UserVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrUser.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let strType = arrUser[indexPath.row] as! String
        let cell : ProfileCell = tableView.dequeueReusableCell(withIdentifier: "profile", for: indexPath) as! ProfileCell
        cell.btnProfiles.addTarget(self, action: #selector(btnProfiles(sender:)), for: .touchUpInside)
        switch strType {
        case CellType.EDITPROFILE.rawValue:
            cell.lblTitle.text = "Account Details"
            cell.btnProfiles.setTitle("EDIT PROFILE", for: .normal)
        case CellType.ADDGOAL.rawValue:
            cell.lblTitle.text = "Study Goals"
            cell.btnProfiles.setTitle("ADD GOAL", for: .normal)
        case CellType.SUBSCRIBE.rawValue:
            cell.lblTitle.text = "Subscription Details"
            cell.btnProfiles.setTitle("SUBSCRIPTION", for: .normal)
        case CellType.CHANGEPASSWORD.rawValue:
            cell.lblTitle.text = "Change Password"
            cell.btnProfiles.setTitle("CHANGE PASSWORD", for: .normal)
        case CellType.SHAREAPP.rawValue:
            cell.lblTitle.text = "Refer & Earn"
            cell.btnProfiles.setTitle("SHARE APP", for: .normal)
        default:
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
}

extension UserVC {
    
    @objc func btnProfiles(sender: UIButton!) {
        let buttonPosition = sender.convert(CGPoint.zero, to: tblUser)
        let indexPath = tblUser.indexPathForRow(at: buttonPosition)!
        let strType = arrUser[indexPath.row] as! String

        switch strType {
        case CellType.EDITPROFILE.rawValue:
            let vc = loadVC(strStoryboardId: SB_PROFILE, strVCId: idEditProfileVC)
            self.navigationController?.pushViewController(vc, animated: true)
        case CellType.ADDGOAL.rawValue:
            let vc = loadVC(strStoryboardId: SB_PROFILE, strVCId: idAddGoalVC)
            self.navigationController?.pushViewController(vc, animated: true)
        case CellType.SUBSCRIBE.rawValue:
            let vc = loadVC(strStoryboardId: SB_PROFILE, strVCId: idSubscriptionDetailsVC) as! SubscriptionDetailsVC
            vc.arrSubscribed = arrSubscribed
            self.navigationController?.pushViewController(vc, animated: true)
        case CellType.CHANGEPASSWORD.rawValue:
            let vc = loadVC(strStoryboardId: SB_PROFILE, strVCId: idChangePasswordVC)
            self.navigationController?.pushViewController(vc, animated: true)
        case CellType.SHAREAPP.rawValue:
            let vc = loadVC(strStoryboardId: SB_PROFILE, strVCId: idShareAppVC)
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            break
        }
    }
}

//MARK:- API CALLING
extension UserVC {
    func apiGetSubscription() {
        activity.startAnimating()
        let parameter:NSDictionary = ["service":APIGetSubscription,
                                      "request":[],
                                      "auth":[
                                        "id":getUserID(),
                                        "token":GetAPIToken()]]
        
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: SERVER_URL,
            service: APIGetSubscription,
            parameters: parameter,
            method: .post,
            showLoader: false)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "not found")
                if responseDict![kSUCCESS] as? String == "1"
                {
                    self.arrSubscribed = arrayOfFilteredBy(arr: responseDict?.object(forKey: kDATA) as! NSArray)
                }
            }
            self.setArray()
            self.tblUser.reloadData()
            self.activity.stopAnimating()
        }
    }
    
    func setArray() {
        arrUser = [CellType.EDITPROFILE.rawValue, CellType.ADDGOAL.rawValue, CellType.SUBSCRIBE.rawValue, CellType.CHANGEPASSWORD.rawValue, CellType.SHAREAPP.rawValue]
        if arrSubscribed.count <= 0 {
            arrUser.remove(CellType.SUBSCRIBE.rawValue)
        }
        
        if dictUser["fbId"] as? String ?? "0" != "0" || dictUser["gId"] as? String ?? "0" != "0" {
            arrUser.remove(CellType.CHANGEPASSWORD.rawValue)
        }
    }
}
