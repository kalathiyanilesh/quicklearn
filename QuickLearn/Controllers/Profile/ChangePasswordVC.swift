//
//  ChangePasswordVC.swift
//  QuickLearn
//
//  Created by NILESH on 26/04/19.
//  Copyright © 2019 NILESH. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController {

    @IBOutlet var txtCurrentPwd: UITextField!
    @IBOutlet var txtNewPwd: UITextField!
    @IBOutlet var txtConfirmPwd: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK:- UIBUTTON ACTIONS
    @IBAction func btnMenu(_ sender: Any) {
        self.revealViewController().revealToggle(animated:true)
    }
    
    @IBAction func btnUpdatePass(_ sender: Any) {
        if (validateTxtFieldLength(txtCurrentPwd, withMessage: EnterOLDPassword)) &&
            (validateTxtFieldLength(txtNewPwd, withMessage: EnterNEWPassword)) &&
            (passwordMismatch(txtNewPwd, txtConfirmPwd, withMessage: PasswordMismatch)) {
            apiChangePasword()
        }
    }
}

//MARK:- API CALLING
extension ChangePasswordVC {

    func apiChangePasword() {
        self.view.endEditing(true)
        let parameter:NSDictionary = ["service":APIChangePass,
                                      "request":["data":[
                                        "p_password":txtCurrentPwd.text!,
                                        "p_newpassword":txtNewPwd.text!,
                                        "p_confpassword":txtConfirmPwd.text!]],
                                      "auth":[
                                        "id":getUserID(),
                                        "token":GetAPIToken()]]
        
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: SERVER_URL,
            service: APIChangePass,
            parameters: parameter,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "not found")
                if responseDict![kSUCCESS] as? String == "1"
                {
                    showMessage(responseDict![kMESSAGE] as! String)
                    let vc = loadVC(strStoryboardId: SB_TABBAR, strVCId: idNavHome)
                    UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                        APP_DELEGATE.window?.rootViewController = vc
                    }, completion: nil)
                } else {
                    showMessage(responseDict![kMESSAGE] as! String)
                }
            }
        }
    }
}
