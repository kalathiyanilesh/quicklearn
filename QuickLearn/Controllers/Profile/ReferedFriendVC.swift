//
//  ReferedFriendVC.swift
//  QuickLearn
//
//  Created by PC on 21/05/19.
//  Copyright © 2019 NILESH. All rights reserved.
//

import UIKit

class ReferedCell: UITableViewCell {
    @IBOutlet weak var imgUserPic: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
}

class ReferedFriendVC: UIViewController {

    @IBOutlet weak var tblFriends: UITableView!
    var arrFrind = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        apiGetReference()
    }
    
    //MARK:- Button Action
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

//MARK:- UITableViewDelegate METHOD
extension ReferedFriendVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFrind.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ReferedCell = tableView.dequeueReusableCell(withIdentifier: "ReferedCell", for: indexPath) as! ReferedCell
        let dictData = arrFrind.object(at: indexPath.row) as! NSDictionary
        cell.lblUserName.text = String(format:"%@ %@", dictData.object(forKey: "clientFirstName") as! String, dictData.object(forKey: "clientLastName") as! String)
        cell.imgUserPic.sd_setImage(with: URL(string: dictData.object(forKey: "clientProfile") as? String ?? ""), placeholderImage: UIImage(named: "default_pic"))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

//MARK:- API CALLING
extension ReferedFriendVC {
    func apiGetReference() {
        let parameter:NSDictionary = ["service":APIGetReference,
                                      "request":[],
                                      "auth":[
                                        "id":getUserID(),
                                        "token":GetAPIToken()]]
        
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: SERVER_URL,
            service: APIGetReference,
            parameters: parameter,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "not found")
                if responseDict![kSUCCESS] as? String == "1"
                {
                    self.arrFrind = arrayOfFilteredBy(arr: responseDict?.object(forKey: kDATA) as! NSArray).mutableCopy() as! NSMutableArray
                    self.tblFriends.reloadData()
                } else {
                    showMessage(responseDict![kMESSAGE] as! String)
                }
            }
        }
    }
}
