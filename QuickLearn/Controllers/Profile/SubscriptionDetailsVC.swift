//
//  SubscriptionDetailsVC.swift
//  QuickLearn
//
//  Created by dhaval on 19/08/19.
//  Copyright © 2019 NILESH. All rights reserved.
//

import UIKit

class CellSubscriptionDetails: UITableViewCell {
    @IBOutlet weak var lblCourseName: UILabel!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
}

class SubscriptionDetailsVC: UIViewController {

    var arrSubscribed = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}

//MARK:- UITableViewDelegate METHOD
extension SubscriptionDetailsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSubscribed.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CellSubscriptionDetails = tableView.dequeueReusableCell(withIdentifier: "CellSubscriptionDetails", for: indexPath) as! CellSubscriptionDetails
        let dictData = arrSubscribed[indexPath.row] as! NSDictionary
        cell.lblCourseName.text = dictData["courseName"] as? String
        let month = Int(dictData["total_month"] as! String)!
        if month == 12 {
            cell.lblMonth.text = "\(month) Year"
        } else {
            cell.lblMonth.text = "\(month) Month"
        }
        cell.lblStartDate.text = "Subscribed on \(dictData["effective_startDt"] as! String)"
        cell.lblEndDate.text = "Will Expire on \(dictData["effective_endDt"] as! String)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 135
    }
}
