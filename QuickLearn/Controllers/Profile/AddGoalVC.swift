//
//  AddGoalVC.swift
//  QuickLearn
//
//  Created by PC on 17/05/19.
//  Copyright © 2019 NILESH. All rights reserved.
//

import UIKit

class AddGoalVC: UIViewController {

    @IBOutlet weak var txtDailyReminder: UITextField!
    @IBOutlet weak var txtExamDate: UITextField!
    @IBOutlet weak var txtRecommendQues: UITextField!
    @IBOutlet weak var SwitchStudyGoal: UISwitch!
    @IBOutlet weak var SwitchReminder: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SetDefaultData()
    }

    //MARK:- BUTTON ACTIONS
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDailyRemind(_ sender: Any) {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "hh:mm a"
        var strTime = dateFormat.string(from: Date())
        let alert = UIAlertController(style: .actionSheet, title: "Select Time")
        alert.addDatePicker(mode: .time, date: nil, minimumDate: nil, maximumDate: nil) { date in
            strTime = dateFormat.string(from: date)
        }
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (actionSheetController) -> Void in
            self.txtDailyReminder.text = strTime
        }))
        alert.show()
    }
    
    @IBAction func btnExamDate(_ sender: Any) {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "MMM dd, yyyy"
        var strDate = dateFormat.string(from: Date())
        let alert = UIAlertController(style: .actionSheet, title: "Select Date")
        alert.addDatePicker(mode: .date, date: nil, minimumDate: Date(), maximumDate: nil) { date in
            strDate = dateFormat.string(from: date)
        }
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (actionSheetController) -> Void in
            self.txtExamDate.text = strDate
        }))
        alert.show()
    }
    
    @IBAction func btnRecommendQues(_ sender: Any) {
        let arrData = ["20", "30", "40", "50", "60", "70", "80", "90", "100"]
        var strVal = arrData[0]
        let alert = UIAlertController(style: .actionSheet, title: nil, message: nil)
        alert.addPickerView(values: [arrData]) { (self, picker, index, values) in
            strVal = arrData[index.row]
        }
        alert.addAction(UIAlertAction(title: "Done", style: .cancel) { (action:UIAlertAction!) in
            self.txtRecommendQues.text = strVal
        })
        alert.show()
    }
    
    @IBAction func btnSave(_ sender: Any) {
        apiAddGoal()
    }
}

//MARK:- SET DEFAULT DATA
extension AddGoalVC {
    func SetDefaultData() {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "MMM dd, yyyy"
        txtExamDate.text = dateFormat.string(from: Date())
        
        dateFormat.dateFormat = "hh:mm a"
        txtDailyReminder.text = dateFormat.string(from: Date())
        
        apiGetUserDetails { (error, responseDict) in
            if error == nil {
                if responseDict![kSUCCESS] as? String == "1"
                {
                    let dictData = responseDict?.object(forKey: kDATA) as! NSDictionary

                    self.SwitchStudyGoal.isOn = dictData["studyGoal"] as! String == "1" ? true : false
                    self.SwitchReminder.isOn = dictData["studyGoalReminder"] as! String == "1" ? true : false
                    
                    let strDailyReminder = dictData["dailyReminderTime"] as! String
                    if strDailyReminder != "00:00:00" || strDailyReminder != "00:00" {
                        let strReminderTime = "1970-01-01 \(strDailyReminder)"
                        self.txtDailyReminder.text = ConvertDateInFormate(d: strReminderTime, "yyyy-MM-dd HH:mm:ss", "hh:mm a")
                    }
                    
                    let strExamDate = dictData["examDate"] as! String
                    if strExamDate != "0000-00-00" {
                        self.txtExamDate.text = ConvertDateInFormate(d: strExamDate, "yyyy-MM-dd", "MMM dd, yyyy")
                    }
            
                    let strPerDay = dictData["perDay"] as! String
                    if Int(strPerDay) ?? 0 <= 0 {
                        self.txtRecommendQues.text = "20"
                    } else {
                        self.txtRecommendQues.text = strPerDay
                    }
                }
            }
        }
    }
}

//MARK:- API CALLING
extension AddGoalVC {
    func apiAddGoal() {
        
        let strReminderDate = "1970-01-01 \(txtDailyReminder.text!)"
        let strReminder = ConvertDateInFormate(d: strReminderDate, "yyyy-MM-dd hh:mm a", "HH:mm")
        let strExamDate = ConvertDateInFormate(d: txtExamDate.text!, "MMM dd, yyyy", "yyyy-MM-dd")

        let parameter:NSDictionary = ["service":APISetGoal,
                                      "request":[
                                        "data":[
                                            "p_study_goal":SwitchStudyGoal.isOn ? "1" : "0",
                                            "p_study_goal_reminder":SwitchReminder.isOn ? "1" : "0",
                                            "p_daily_reminder_time":strReminder,
                                            "p_exam_date":strExamDate,
                                            "p_per_day":txtRecommendQues.text!]],
                                      "auth":[
                                        "id":getUserID(),
                                        "token":GetAPIToken()]]

        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: SERVER_URL,
            service: APISetGoal,
            parameters: parameter,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "not found")
                if responseDict![kSUCCESS] as? String == "1"
                {
                    showMessage(responseDict![kMESSAGE] as! String)
                    self.navigationController?.popViewController(animated: true)
                } else {
                    showMessage(responseDict![kMESSAGE] as! String)
                }
            }
        }
    }
}
