//
//  ShareAppVC.swift
//  QuickLearn
//
//  Created by PC on 21/05/19.
//  Copyright © 2019 NILESH. All rights reserved.
//

import UIKit

class ShareAppVC: UIViewController {

    @IBOutlet weak var lblReferralCode: UILabel!
    @IBOutlet weak var viewCode: UIView!
    @IBOutlet weak var lblWalletBlnc: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let dictUser = getUserInfo()
        
        lblReferralCode.text = dictUser.object(forKey: "promotionCode") as? String
        lblWalletBlnc.text = String(format:"%.2f",Double(dictUser["walletBalance"] as! String) ?? 0)
        
        let border = CAShapeLayer()
        border.strokeColor = UIColor.darkGray.cgColor
        border.fillColor = nil
        border.lineDashPattern = [4, 4]
        border.path = UIBezierPath(rect: viewCode.bounds).cgPath
        border.frame = viewCode.bounds
        viewCode.layer.addSublayer(border)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        apiGetUserDetails { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "not found")
                if responseDict![kSUCCESS] as? String == "1"
                {
                    let dictData = responseDict?.object(forKey: kDATA) as! NSDictionary
                    self.lblWalletBlnc.text = String(format:"%.2f",Double(dictData["walletBalance"] as! String) ?? 0)
                }
            }
        }
    }

    //MARK:- BUTTON ACTIONS
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnViewPastRefer(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_PROFILE, strVCId: idReferedFriendVC)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnReferNow(_ sender: Any) {
        let strMsg = "Hi!, I'm playing this cool new app QPApp. Use my referral code: \(lblReferralCode.text!) and sign up: https://www.google.com"
        let textToShare = [ strMsg ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        self.present(activityViewController, animated: true, completion: nil)
    }
    
}
