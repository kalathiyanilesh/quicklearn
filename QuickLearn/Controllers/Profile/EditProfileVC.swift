//
//  EditProfileVC.swift
//  QuickLearn
//
//  Created by NILESH on 27/04/19.
//  Copyright © 2019 NILESH. All rights reserved.
//

import UIKit

class EditProfileVC: UIViewController {

    @IBOutlet weak var imgProfilePic: UIImageView!
    @IBOutlet weak var txtFName: UITextField!
    @IBOutlet weak var txtLName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhoneNo: UITextField!
    @IBOutlet weak var txtDateofBirth: UITextField!
    @IBOutlet weak var txtState: UITextField!
    
    var strStateID = ""
    var strProfilePic = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let dictUser = getUserInfo()
        txtFName.text = dictUser["clientFirstName"] as? String
        txtLName.text = dictUser["clientLastName"] as? String
        txtEmail.text = dictUser["cleintEmail"] as? String
        txtPhoneNo.text = dictUser["mobileNo"] as? String
        txtDateofBirth.text = dictUser["dateOfBirth"] as? String
        imgProfilePic.sd_setImage(with: URL(string: dictUser.object(forKey: "clientProfile") as? String ?? ""), placeholderImage: UIImage(named: "default_pic"))
        strStateID = dictUser["stateId"] as! String
        
        let pred : NSPredicate = NSPredicate(format: "stateId = %@", strStateID)
        let arrTemp = (APP_DELEGATE.arrStates.filtered(using: pred) as NSArray).mutableCopy() as! NSMutableArray
        if arrTemp.count > 0 {
            let dictState = arrTemp[0] as! NSDictionary
            self.txtState.text = dictState["stateName"] as? String
        }
        
        if txtDateofBirth.text == "0000-00-00" {
            txtDateofBirth.text = ""
        }
    }
    
    //MARK:- UIBUTON ACTIONS
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnChange(_ sender: Any) {
        ImagePickerManager().pickImage(self, true) { image in
            self.imgProfilePic.image = image
            apiUploadImage(image, responseData: { (error, responseDict) in
                if error == nil {
                    print(responseDict ?? "Not Found")
                    if responseDict![kSUCCESS] as? String == "1" {
                        self.strProfilePic = responseDict?["data"] as? String ?? ""
                    }
                }
            })
        }
    }
    
    @IBAction func btnDateOfBIrth(_ sender: Any) {
        self.view.endEditing(true)
        let alert = UIAlertController(style: .actionSheet, title: "Select Birth Date")
        alert.addDatePicker(mode: .date, date: Date(), minimumDate: nil, maximumDate: Date()) { date in
            let dateFormat = DateFormatter()
            dateFormat.dateFormat = "yyyy-MM-dd"
            self.txtDateofBirth.text = dateFormat.string(from: date)
        }
        alert.addAction(title: "OK", style: .cancel)
        alert.show()
    }
    
    @IBAction func btnState(_ sender: Any) {
        self.view.endEditing(true)
        let arrData = APP_DELEGATE.arrStates.value(forKeyPath: "stateName") as! NSArray
        if arrData.count > 0 {
            var strVal = arrData[0]
            var strSID = (APP_DELEGATE.arrStates[0] as! NSDictionary)["stateId"] as! String
            let alert = UIAlertController(style: .actionSheet, title: nil, message: nil)
            alert.addPickerView(values: [arrData as! Array<String>]) { (self, picker, index, values) in
                strVal = arrData[index.row]
                strSID = (APP_DELEGATE.arrStates[index.row] as! NSDictionary)["stateId"] as! String
            }
            alert.addAction(UIAlertAction(title: "Done", style: .cancel) { (action:UIAlertAction!) in
                self.txtState.text = strVal as? String
                self.strStateID = strSID
                print(self.strStateID)
            })
            alert.show()
        } else {
            showMessage("Try Again!")
            apiGetStateList()
        }
    }
    
    @IBAction func btnUpdate(_ sender: Any) {
        if validateTxtFieldLength(txtFName, withMessage: EnterFname) &&
            validateTxtFieldLength(txtLName, withMessage: EnterLname) &&
            validateTxtFieldLength(txtDateofBirth, withMessage: EnterBirthDate) &&
            validateTxtFieldLength(txtState, withMessage: EnterState) {
            apiEditProfile()
        }
    }
}

//MARK:- API CALLING
extension EditProfileVC {
    func apiEditProfile() {
        self.view.endEditing(true)
        let parameter:NSDictionary = ["service":APIEditProfile,
                                      "request":["data":[
                                        "p_clientFirstName":txtFName.text!,
                                        "p_clientLastName":txtLName.text!,
                                        "p_dateOfBirth":txtDateofBirth.text!,
                                        "p_clientProfile":strProfilePic,
                                        "p_sex":"M",
                                        "p_clientMiddleName":"abc",
                                        "p_stateId":strStateID]],
                                      "auth":[
                                        "id":getUserID(),
                                        "token":GetAPIToken()]]
        
        HttpRequestManager.sharedInstance.requestWithJsonParam(
            endpointurl: SERVER_URL,
            service: APIEditProfile,
            parameters: parameter,
            method: .post,
            showLoader: true)
        { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "not found")
                if responseDict![kSUCCESS] as? String == "1"
                {
                    let dictData = dictionaryOfFilteredBy(dict: responseDict?.object(forKey: kDATA) as! NSDictionary)
                    let dictUser = getUserInfo()
                    let dictTemp = dictUser.mutableCopy() as! NSMutableDictionary
                    dictTemp.setObject(self.txtFName.text!, forKey: "clientFirstName" as NSCopying)
                    dictTemp.setObject("", forKey: "clientMiddleName" as NSCopying)
                    dictTemp.setObject(self.txtLName.text!, forKey: "clientLastName" as NSCopying)
                    dictTemp.setObject(self.txtDateofBirth.text!, forKey: "dateOfBirth" as NSCopying)
                    dictTemp.setObject(dictData.object(forKey: "clientProfile") as! String, forKey: "clientProfile" as NSCopying)
                    dictTemp.setObject(self.strStateID, forKey: "stateId" as NSCopying)
                    saveInUserDefault(obj: dictTemp.copy() as! NSDictionary, key: UD_UserData)
                    showMessage(responseDict![kMESSAGE] as! String)
                    self.navigationController?.popViewController(animated: true)
                } else {
                    showMessage(responseDict![kMESSAGE] as! String)
                }
            }
        }
    }
}
