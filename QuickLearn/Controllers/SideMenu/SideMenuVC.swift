//
//  SideMenuVC.swift
//  QuickLearn
//
//  Created by NILESH on 26/04/19.
//  Copyright © 2019 NILESH. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {
    @IBOutlet var imgMenu: UIImageView!
    @IBOutlet var lblMenu: UILabel!
}

class SideMenuVC: UIViewController {

    @IBOutlet var tblMenu: UITableView!
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var lblUserName: UILabel!
    
    var arrSideMenu = NSMutableArray()
    var dictUser = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tap = UITapGestureRecognizer(target: self, action: #selector(SideMenuVC.tapImageUser))
        imgUser.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        dictUser = getUserInfo()
        arrSideMenu = ["Home","Change Password","About","Support","Logout"]
        if dictUser["fbId"] as? String ?? "" != "" || dictUser["gId"] as? String ?? "" != "" {
            arrSideMenu.remove("Change Password")
        }
        
        tblMenu.reloadData()
        
        lblUserName.text = String(format:"%@ %@",dictUser.object(forKey: "clientFirstName") as! String,dictUser.object(forKey: "clientLastName") as! String)
        imgUser.sd_setImage(with: URL(string: dictUser.object(forKey: "clientProfile") as? String ?? ""), placeholderImage: UIImage(named: "default_pic"))
        
        tblMenu.isUserInteractionEnabled = true
        tblMenu.reloadData()
    }
    
    @objc func tapImageUser(sender:UITapGestureRecognizer) {
        let vc = loadVC(strStoryboardId: SB_PROFILE, strVCId: idEditProfileVC)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- UITableView DELEGATE METHOD
extension SideMenuVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSideMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : MenuCell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! MenuCell
        cell.lblMenu.text = arrSideMenu.object(at: indexPath.row) as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tblMenu.isUserInteractionEnabled = false
        let selectedMenu = arrSideMenu.object(at: indexPath.row) as! String
        switch selectedMenu {
        case "Home":
            let vc = loadVC(strStoryboardId: SB_TABBAR, strVCId: idSWRevealViewController)
            navigateToView(controller: vc)
            break
        case "Change Password":
            let vc = loadVC(strStoryboardId: SB_PROFILE, strVCId: idChangePasswordVC)
            navigateToView(controller: vc)
            break
        case "Logout":
            tblMenu.isUserInteractionEnabled = true
            let alert = UIAlertController(title: "Are you sure you want to logout?", message: nil, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (alertAction) in
                LogoutUser()
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (alertAction) in
            }))
            self.present(alert, animated: true, completion: nil)
            break
        default:
            tblMenu.isUserInteractionEnabled = true
            break
        }
        tblMenu.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func navigateToView(controller : UIViewController) {
        let navigate : UINavigationController = UINavigationController.init(rootViewController: controller)
        navigate.navigationBar.isHidden = true
        self.revealViewController().pushFrontViewController(navigate, animated: true)
    }
}
