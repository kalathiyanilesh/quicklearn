//
//  LinkViewCell.swift
//  QuickLearn
//
//  Created by dhaval on 15/06/19.
//  Copyright © 2019 NILESH. All rights reserved.
//

import UIKit
import URLEmbeddedView

class LinkViewCell: UITableViewCell {

    @IBOutlet var viewSubscribe: UIView!
    @IBOutlet var vwLinkAddress: UIView!
    @IBOutlet var embeddedView: URLEmbeddedView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
