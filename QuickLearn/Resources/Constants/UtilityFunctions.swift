//
//  constant.swift

import Foundation
import UIKit
import CoreLocation
import SystemConfiguration
import AVFoundation
import SwiftMessages
import FBSDKLoginKit

var spinner = RPLoadingAnimationView.init(frame: CGRect.zero)
var overlayView = UIView()
var lat_currnt: Double = 0
var long_currnt: Double = 0
var GlobalTakenTime: Double = 0
var GlobalTimerAnswer: Timer?

enum QAMode: Int {
    case LEARNINGMODE
    case SKILLTESTMODE
}

public let isSimulator: Bool = {
    var isSim = false
    #if arch(i386) || arch(x86_64)
        isSim = true
    #endif
    return isSim
}()

public func FONT_REGULAR(fontSize: CGFloat) -> UIFont {
    return UIFont(name: "Roboto-Regular", size: fontSize)!
}

public func FONT_MEDIUMBOLD(fontSize: CGFloat) -> UIFont{
    return UIFont(name: "Roboto-Medium", size: fontSize)!
}

public func FONT_BOLD(fontSize: CGFloat) -> UIFont{
    return UIFont(name: "Roboto-Bold", size: fontSize)!
}

//MARK:-  Get VC for navigation
//var banner = NotificationBanner(title: "", subtitle: "", style: .success)
public func getStoryboard(storyboardName: String) -> UIStoryboard {
    return UIStoryboard(name: storyboardName, bundle: nil)
}

public func loadVC(strStoryboardId: String, strVCId: String) -> UIViewController {
    let vc = getStoryboard(storyboardName: strStoryboardId).instantiateViewController(withIdentifier: strVCId)
    return vc
}

public var CurrentTimeStamp: String {
    return "\(NSDate().timeIntervalSince1970 * 1000)"
}

func convertTextToQRCode(text: String) -> UIImage {
    let size : CGSize = CGSize(width: 500, height: 500)
    let data = text.data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
    let filter = CIFilter(name: "CIQRCodeGenerator")!
    filter.setValue(data, forKey: "inputMessage")
    filter.setValue("L", forKey: "inputCorrectionLevel")
    let qrcodeCIImage = filter.outputImage!
    let cgImage = CIContext(options:nil).createCGImage(qrcodeCIImage, from: qrcodeCIImage.extent)
    UIGraphicsBeginImageContext(CGSize(width: size.width * UIScreen.main.scale, height:size.height * UIScreen.main.scale))
    let context = UIGraphicsGetCurrentContext()
    context!.interpolationQuality = .none
    context?.draw(cgImage!, in: CGRect(x: 0.0,y: 0.0,width: context!.boundingBoxOfClipPath.width,height: context!.boundingBoxOfClipPath.height))
    let preImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    let qrCodeImage = UIImage(cgImage: (preImage?.cgImage!)!, scale: 1.0/UIScreen.main.scale, orientation: .downMirrored)
    return qrCodeImage
}

func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
  return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
}

func saveInUserDefault(obj: AnyObject, key: String) {
    UserDefaults.standard.set(obj, forKey: key)
    UserDefaults.standard.synchronize()
}

func getUserInfo() -> NSDictionary {
    var dictUser: NSDictionary = NSDictionary()
    if let dictData = UserDefaults.standard.value(forKey: UD_UserData) as? NSDictionary {
        dictUser = dictData
    }
    return dictUser
}

func getUserID() -> String {
    if(UserDefaults.standard.string(forKey: UD_UserId) == nil) {
        return ""
    } else {
        return UserDefaults.standard.string(forKey: UD_UserId)!
    }
}

func getDeviceToken() -> String {
    if(UserDefaults.standard.string(forKey: UD_DeviceToken) == nil) {
        return "123456"
    } else {
        return UserDefaults.standard.string(forKey: UD_DeviceToken)!
    }
}

func GetAPIToken() -> String {
    if(UserDefaults.standard.string(forKey: UD_APIToken) == nil) {
        return ""
    } else {
        return UserDefaults.standard.string(forKey: UD_APIToken)!
    }
}

func GetSecretLogID() -> String {
    if(UserDefaults.standard.string(forKey: UD_SecretLogID) == nil) {
        return ""
    } else {
        return UserDefaults.standard.string(forKey: UD_SecretLogID)!
    }
}

func SaveUserData(_ dictData: NSDictionary, token: String, secretLogId: String) {
    saveInUserDefault(obj: dictData, key: UD_UserData)
    if token.count > 0 {
        saveInUserDefault(obj: token as AnyObject, key: UD_APIToken)
    }
    if secretLogId.count > 0 {
        saveInUserDefault(obj: secretLogId as AnyObject, key: UD_SecretLogID)
    }
    saveInUserDefault(obj: dictData.object(forKey: "clientId") as! String as AnyObject, key: UD_UserId)
    apiGetStateList()
 }

func LogoutUser() {
    let parameter:NSDictionary = ["service":APILogout,
                                  "request":["data":[
                                    "secret_log_id":GetSecretLogID()]],
                                  "auth":[
                                    "id":getUserID(),
                                    "token":GetAPIToken()]]
    
    HttpRequestManager.sharedInstance.requestWithJsonParam(
        endpointurl: SERVER_URL,
        service: APILogout,
        parameters: parameter,
        method: .post,
        showLoader: true)
    { (error, responseDict) in
        if error == nil {
            print(responseDict ?? "not found")
            if responseDict![kSUCCESS] as? String == "1"
            {
                CleanData()
                let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: idNavMain)
                UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                    APP_DELEGATE.window?.rootViewController = vc
                }, completion: nil)
            } else {
                showMessage(responseDict![kMESSAGE] as! String)
            }
        }
    }
}

func CleanData() {
    FBSDKLoginManager().logOut()
    UserDefaults.standard.removeObject(forKey: UD_UserData)
    UserDefaults.standard.removeObject(forKey: UD_APIToken)
    UserDefaults.standard.removeObject(forKey: UD_UserId)
}

func compressImage(image:UIImage) -> NSData
{
    var compression:CGFloat!
    let maxCompression:CGFloat!
    compression = 0.9
    maxCompression = 0.1
    var imageData = UIImageJPEGRepresentation(image, compression)! as NSData
    while (imageData.length > 10 && compression > maxCompression)
    {
        compression = compression - 0.10
        imageData = UIImageJPEGRepresentation(image, compression)! as NSData
    }
    return imageData
}

func SetCursorColor() {
    UITextField.appearance().tintColor = APP_COLOR
    UITextView.appearance().tintColor = APP_COLOR
}

func setNoDataLabel(tableView:UITableView, array:NSMutableArray, text:String) -> Int
{
    var numOfSections = 0
    
    if array.count != 0 {
        tableView.backgroundView = nil
        numOfSections = 1
    } else {
        let noDataLabel = UILabel()
        noDataLabel.frame = CGRect(x: 10, y: 0, width: tableView.frame.size.width-20, height: tableView.frame.size.height)
        noDataLabel.text = text
        noDataLabel.numberOfLines = 0
        noDataLabel.font = FONT_MEDIUMBOLD(fontSize: 20)
        noDataLabel.textColor = UIColor.lightGray.withAlphaComponent(0.5)
        noDataLabel.textAlignment = NSTextAlignment.center
        tableView.backgroundView = noDataLabel
        tableView.separatorStyle = .none
    }
    
    return numOfSections
}

func changeDateFormat(strDate: String,FormatFrom: String, FormateTo: String) -> String {
    let dateFormat = DateFormatter()
    dateFormat.dateFormat = FormatFrom
    let date = dateFormat.date(from: strDate)
    dateFormat.dateFormat = FormateTo
    if date == nil {
        return strDate
    }
    return dateFormat.string(from: date!)
}

public func convert(dicData:NSDictionary,toString:(_ strData:String)->())
{
    do {
        let jsonData = try JSONSerialization.data(withJSONObject: dicData)
        if let json = String(data: jsonData, encoding: .utf8) {
            toString(json)
        }
    } catch {
        print("something went wrong with parsing json")
    }
}

public func convert(JSONstring:String,toDictionary:(_ strData:NSDictionary)->())
{
    var strJSON:String = JSONstring
    strJSON = strJSON.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    let arr = strJSON.components(separatedBy: "\n")
    var dict : [String:Any]?
    for jsonString in arr{
        if let jsonDataToVerify = jsonString.data(using: String.Encoding.utf8)
        {
            do {
                dict = try JSONSerialization.jsonObject(with: jsonDataToVerify) as? [String : Any]
                print("JSON is valid.")
                toDictionary(dict! as NSDictionary)
            } catch {
                //print("Error deserializing JSON: \(error.localizedDescription)")
            }
        }
    }
}

public func getMainBaseCoin(strCoin:String,to:(_ strMain:String, _ strBase:String)->())
{
    if strCoin.count > 0 {
        let arrCoins = strCoin.components(separatedBy: "/")
        if arrCoins.count == 2 {
            to(arrCoins[0],arrCoins[1])
        }
    }
}

func dictionaryOfFilteredBy(dict: NSDictionary) -> NSDictionary {
    let replaced: NSMutableDictionary = NSMutableDictionary(dictionary : dict)
    let blank: String = ""
    for (key, _) in dict
    {
        let object = dict[key] as AnyObject
        
        if (object.isKind(of: NSNull.self))
        {
            replaced[key] = blank as AnyObject?
        }
        else if (object is [AnyHashable: AnyObject])
        {
            replaced[key] = dictionaryOfFilteredBy(dict: object as! NSDictionary)
        }
        else if (object is [AnyObject])
        {
            replaced[key] = arrayOfFilteredBy(arr: object as! NSArray)
        }
        else
        {
            replaced[key] = "\(object)" as AnyObject?
        }
    }
    return replaced
}

func arrayOfFilteredBy(arr: NSArray) -> NSArray {
    let replaced: NSMutableArray = NSMutableArray(array: arr)
    let blank: String = ""
    
    for i in 0..<arr.count
    {
        let object = arr[i] as AnyObject
        
        if (object.isKind(of: NSNull.self))
        {
            replaced[i] = blank as AnyObject
        }
        else if (object is [AnyHashable: AnyObject])
        {
            replaced[i] = dictionaryOfFilteredBy(dict: arr[i] as! NSDictionary)
        }
        else if (object is [AnyObject])
        {
            replaced[i] = arrayOfFilteredBy(arr: arr[i] as! NSArray)
        }
        else
        {
            replaced[i] = "\(object)" as AnyObject
        }
        
    }
    return replaced
}

public var mostTopViewController: UIViewController? {
    get {
        return UIApplication.shared.keyWindow?.rootViewController
    }
    set {
        UIApplication.shared.keyWindow?.rootViewController = newValue
    }
}

//MARK:- iOS version checking Functions
public var appDisplayName: String? {
    return Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String
}
public var appBundleID: String? {
    return Bundle.main.bundleIdentifier
}
public func IOS_VERSION() -> String {
    return UIDevice.current.systemVersion
}
public var statusBarHeight: CGFloat {
    return UIApplication.shared.statusBarFrame.height
}
public var applicationIconBadgeNumber: Int {
    get {
        return UIApplication.shared.applicationIconBadgeNumber
    }
    set {
        UIApplication.shared.applicationIconBadgeNumber = newValue
    }
}
public var appVersion: String? {
    return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
}
public func SCREENWIDTH() -> CGFloat
{
    let screenSize = UIScreen.main.bounds
    return screenSize.width
}

public func SCREENHEIGHT() -> CGFloat
{
    let screenSize = UIScreen.main.bounds
    return screenSize.height
}

//MARK:- SwiftLoader
public func showLoaderHUD(strMessage:String)
{
    let activityData = ActivityData()
    NVActivityIndicatorView.DEFAULT_TYPE = .ballPulse
    NVActivityIndicatorView.DEFAULT_COLOR = UIColor.white
    NVActivityIndicatorView.DEFAULT_BLOCKER_MESSAGE = ""
    NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
}

public func hideLoaderHUD()
{
    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    spinner.removeFromSuperview()
    overlayView.removeFromSuperview()
}

public func showMessage(_ bodymsg:String)
{
    let view = MessageView.viewFromNib(layout: .cardView)
    //view.configureDropShadow()
    view.configureContent(body: bodymsg)
    view.layoutMarginAdditions = UIEdgeInsets(top: 25, left: 10, bottom: 20, right: 10)
    view.configureTheme(.warning, iconStyle: .light)
    
    let backgroundColor = Color_Hex(hex: "EAEBEF")
    let foregroundColor = UIColor.black
    
    view.configureTheme(backgroundColor: backgroundColor, foregroundColor: foregroundColor)
    view.titleLabel?.isHidden = true
    view.button?.isHidden = true
    view.iconImageView?.isHidden = true
    view.iconLabel?.isHidden = true
    SwiftMessages.show(view: view)
}

//MARK:- Network indicator
public func ShowNetworkIndicator(xx :Bool)
{
    runOnMainThreadWithoutDeadlock {
        UIApplication.shared.isNetworkActivityIndicatorVisible = xx
    }
}

//MARK : Length validation
public func TRIM(string: Any) -> String
{
    return (string as AnyObject).trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
}

public func validateTxtFieldLength(_ txtVal: UITextField, withMessage msg: String) -> Bool {
    if TRIM(string: txtVal.text ?? "").count == 0
    {
        txtVal.text = TRIM(string: txtVal.text ?? "")
        txtVal.shake()
        showMessage(msg)
        return false
    }
    return true
}

public func validateTxtViewLength(_ txtVal: UITextView, withMessage msg: String) -> Bool {
    if TRIM(string: txtVal.text ?? "").count == 0
    {
        txtVal.text = TRIM(string: txtVal.text ?? "")
        txtVal.shake()
        showMessage(msg)
        return false
    }
    return true
}

public func validateTextLength(_ strVal: String, withMessage msg: String) -> Bool {
    if TRIM(string: strVal).count == 0
    {
        showMessage(msg)
        return false
    }
    return true
}

public func validateAmount(_ txtVal: UITextField, withMessage msg: String) -> Bool {
    var amount : Double = 0
    if TRIM(string: txtVal.text!).count > 0{
        amount = Double(TRIM(string: txtVal.text ?? ""))!
    }
    if amount <= 0 {
        txtVal.text = TRIM(string: txtVal.text ?? "")
        txtVal.shake()
        showMessage(msg)
        return false
    }
    return true
}

public func validateMinTxtFieldLength(_ txtVal: UITextField, withMessage msg: String) -> Bool {
    if TRIM(string: txtVal.text ?? "").count < 6
    {
        txtVal.text = TRIM(string: txtVal.text ?? "")
        txtVal.shake()
        showMessage(msg)
        return false
    }
    print(">6 length")
    return true
}

public func validateMaxTxtFieldLength(_ txtVal: UITextField, lenght:Int,msg: String) -> Bool
{
    if TRIM(string: txtVal.text ?? "").count > lenght
    {
        showMessage(msg)
        return false
    }
    return true
}

public func passwordMismatch(_ txtVal: UITextField, _ txtVal1: UITextField, withMessage msg: String) -> Bool
{
    if TRIM(string: txtVal.text ?? "") != TRIM(string: txtVal1.text ?? "")
    {
        showMessage(msg);
        return false
    }
    return true
}

public func validateEmailAddress(_ txtVal: UITextField ,withMessage msg: String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    if(emailTest.evaluate(with: txtVal.text) != true)
    {
        showMessage(msg);
        return false
    }
    return true
}

public func validatePhoneNo(_ txtVal: UITextField ,withMessage msg: String) -> Bool
{
    let PHONE_REGEX = "^[0-9]{6,}$"
    let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
    if(phoneTest.evaluate(with: txtVal.text) != true)
    {
        showMessage(msg);
        return false
    }
    return true
}

public func validateTermsCondition(_ termsSelected: Bool, withMessage msg: String) -> Bool {
    if !termsSelected
    {
        showMessage(msg)
        return false
    }
    return true
}

public func validateBool(_ isSelected: Bool, withMessage msg: String) -> Bool {
    if !isSelected
    {
        showMessage(msg)
        return false
    }
    return true
}

public func isBase64(stringBase64:String) -> Bool
{
    let regex = "([A-Za-z0-9+/]{4})*" + "([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)"
    let test = NSPredicate(format:"SELF MATCHES %@", regex)
    if(test.evaluate(with: stringBase64) != true)
    {
        return false
    }
    return true
}

public func getDateString(_ date: Date,format: String) -> String {
    let dateFormat = DateFormatter()
    dateFormat.dateFormat = format
    return dateFormat.string(from: date)
}

public func isUserLogin() -> Bool {
    if UserDefaults.standard.value(forKey: UD_UserData) == nil{
        return false
    }
    return true
}

//MARK:- - Get image from image name
public func Set_Local_Image(imageName :String) -> UIImage
{
    return UIImage(named:imageName)!
}

//MARK:- FONT
public func FontWithSize(_ fname: String,_ fsize: Int) -> UIFont
{
    return UIFont(name: fname, size: CGFloat(fsize))!
}

public func randomColor() -> UIColor {
    let r: UInt32 = arc4random_uniform(255)
    let g: UInt32 = arc4random_uniform(255)
    let b: UInt32 = arc4random_uniform(255)
    
    return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: 1.0)
}

//Mark : string to dictionary
public func convertStringToDictionary(str:String) -> [String: Any]? {
    if let data = str.data(using: .utf8) {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        } catch {
            print(error.localizedDescription)
        }
    }
    return nil
}

//MARK: dictionary to string
public func convertDictionaryToJSONString(dic:NSDictionary) -> String? {
    do{
        let jsonData: Data? = try JSONSerialization.data(withJSONObject: dic, options: [])
        var myString: String? = nil
        if let aData = jsonData {
            myString = String(data: aData, encoding: .utf8)
        }
        return myString
    }catch{
        print(error)
    }
    return ""
}

//MARK:-Check Internet connection
func isConnectedToNetwork() -> Bool
{
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
            SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
        }
    })
    else
    {
        return false
    }
    
    var flags : SCNetworkReachabilityFlags = []
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
        return false
    }
    
    let isReachable = flags.contains(.reachable)
    let needsConnection = flags.contains(.connectionRequired)
    let available =  (isReachable && !needsConnection)
    if(available)
    {
        return true
    }
    else
    {
        showMessage(InternetNotAvail)
        return false
    }
}

func Color_Hex(hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

func ConvertDateInFormate(d: String, _ from: String, _ to: String) -> String {
    let dateFormat = DateFormatter()
    dateFormat.dateFormat = from
    let ReminderDate = dateFormat.date(from: d)
    dateFormat.dateFormat = to
    return dateFormat.string(from: ReminderDate!)
}

func popBack<T: UIViewController>(_ navController: UINavigationController?, toControllerType: T.Type) {
    if var viewControllers: [UIViewController] = navController?.viewControllers {
        viewControllers = viewControllers.reversed()
        for currentViewController in viewControllers {
            if currentViewController .isKind(of: toControllerType) {
                navController?.popToViewController(currentViewController, animated: true)
                break
            }
        }
    }
}

//MARK:- FIND HEIGHT OR WIDTH
extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}

extension String {
    func Localized() -> String {
        return NSLocalizedString(self, comment: "")
    }
    
    var firstUppercased: String {
        return prefix(1).uppercased() + dropFirst()
    }
    
    var firstCapitalized: String {
        return String(prefix(1)).capitalized + dropFirst()
    }
}

extension TimeInterval {
    var minuteSecond: String {
        return String(format: "%dm %ds", minute, second)
    }
    var minute: Int {
        return Int((self/60).truncatingRemainder(dividingBy: 60))
    }
    var second: Int {
        return Int(truncatingRemainder(dividingBy: 60))
    }
    var millisecond: Int {
        return Int((self*1000).truncatingRemainder(dividingBy: 1000))
    }
}

//MARK:- PRIVATE METHOD
func apiGetStateList() {
    let parameter:NSDictionary = ["service":APIGetState,
                                  "request":[],
                                  "auth":[
                                    "id":getUserID(),
                                    "token":GetAPIToken()]]
    
    HttpRequestManager.sharedInstance.requestWithJsonParam(
        endpointurl: SERVER_URL,
        service: APIGetState,
        parameters: parameter,
        method: .post,
        showLoader: false)
    { (error, responseDict) in
        if error == nil {
            print(responseDict ?? "not found")
            if responseDict![kSUCCESS] as? String == "1"
            {
                APP_DELEGATE.arrStates = arrayOfFilteredBy(arr: responseDict?.object(forKey: kDATA) as! NSArray).mutableCopy() as! NSMutableArray
                print(APP_DELEGATE.arrStates)
            }
        }
    }
}

func apiUploadImage(_ image: UIImage,
                    responseData:@escaping  (_ error: NSError?,_ responseDict: NSDictionary?) -> Void) {
    let imgData = compressImage(image: image)
    let parameter:NSDictionary = ["user_image":imgData, "token_id":GetAPIToken()]

    HttpRequestManager.sharedInstance.requestWithPostMultipartParam(
        endpointurl: UPLOAD_IMAGE_URL,
        showLoader: true,
        parameters: parameter) { (error, responseDict) in
            if error == nil {
                print(responseDict ?? "not found")
                responseData(nil, responseDict)
            } else {
                responseData(error, nil)
            }
    }
}

func apiGetUserDetails(responseData:@escaping  (_ error: NSError?,_ responseDict: NSDictionary?) -> Void) {
    let parameter:NSDictionary = ["service":APIGetUserDetails,
                                  "request":[],
                                  "auth":[
                                    "id":getUserID(),
                                    "token":GetAPIToken()]]

    HttpRequestManager.sharedInstance.requestWithJsonParam(
        endpointurl: SERVER_URL,
        service: APIGetUserDetails,
        parameters: parameter,
        method: .post,
        showLoader: false)
    { (error, responseDict) in
        if error == nil {
            print(responseDict ?? "not found")
            if responseDict![kSUCCESS] as? String == "1"
            {
                let dictData = responseDict?.object(forKey: kDATA) as! NSDictionary
                saveInUserDefault(obj: dictData, key: UD_UserData)
            }
            responseData(nil, responseDict)
        } else {
            responseData(error, nil)
        }
    }
}

func SaveSelectedMode(_ type: Int, _ SubjectID: String) {
    let sID = "\(getUserID())_\(SubjectID)"
    let dictMode: NSDictionary = ["sID":sID,"type": type]
    var arrData = NSMutableArray()
    if let arrTmp = UserDefaults.standard.object(forKey: SELECTED_MODE) as? NSArray {
        arrData = arrTmp.mutableCopy() as! NSMutableArray
        let pred : NSPredicate = NSPredicate(format: "sID = %@", sID)
        let arrTemp = arrData.filtered(using: pred) as NSArray
        if arrTemp.count > 0 {
            arrData.remove(arrTemp[0] as! NSDictionary)
        }
    }
    
    arrData.add(dictMode)

    saveInUserDefault(obj: arrData, key: SELECTED_MODE)
    showMessage("Mode Change Successfully!")
}

func SetSelectedModeFlag(_ SubjectID: String) {
    APP_DELEGATE.isLearnMode = false
    if let arrTmp = UserDefaults.standard.object(forKey: SELECTED_MODE) as? NSArray {
        let sID = "\(getUserID())_\(SubjectID)"
        let pred : NSPredicate = NSPredicate(format: "sID = %@", sID)
        let arrData = arrTmp.filtered(using: pred) as NSArray
        if arrData.count > 0 {
            let dict = arrData[0] as! NSDictionary
            let value = dict.object(forKey: "type") as! Int
            if value == 0 {
                APP_DELEGATE.isLearnMode = true
            }
        }
    }
}

func isModeSelected(_ SubjectID: String) -> Bool {
    if let arrTmp = UserDefaults.standard.object(forKey: SELECTED_MODE) as? NSArray {
        let sID = "\(getUserID())_\(SubjectID)"
        let pred : NSPredicate = NSPredicate(format: "sID = %@", sID)
        let arrTemp = arrTmp.filtered(using: pred) as NSArray
        if arrTemp.count > 0 {
            return true
        }
    }
    return false
}

func GetSelectedMode(_ SubjectID: String) -> Int {
    if let arrTmp = UserDefaults.standard.object(forKey: SELECTED_MODE) as? NSArray {
        let sID = "\(getUserID())_\(SubjectID)"
        let pred : NSPredicate = NSPredicate(format: "sID = %@", sID)
        let arrData = arrTmp.filtered(using: pred) as NSArray
        if arrData.count > 0 {
            let dict = arrData[0] as! NSDictionary
            let value = dict.object(forKey: "type") as! Int
            return value
        }
    }
    return 0
}
