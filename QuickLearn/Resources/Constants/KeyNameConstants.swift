//
//  KeyNamesConstants.swift
//  ELFramework
//
//  Created by Admin on 14/12/17.
//  Copyright © 2017 EL. All rights reserved.
//

import Foundation
import UIKit

let APP_DELEGATE = UIApplication.shared.delegate as! AppDelegate

let SELECTED_MODE = "selectedmode"

//MARK:- APPNAME
public let APPNAME :String = "QuickLearn"

//MARK:- LOADER COLOR
let loadercolor = [Color_Hex(hex: "1C2244"), Color_Hex(hex: "131835")]

//MARK:- PLACEHOLDER IMAGES
let QUES_PLACE_HOLDER = UIImage(named: "placeholder")

//MARK:- HEADER KEY
public let kHEADER = "token"
public let kTIMEZONE = "timezone"

//MARK:- PAGELIMIT (PAGINATION)
public let PAGE_LIMIT = 10

//MARK:- COLORDS
let APP_COLOR = Color_Hex(hex: "8F6BFF")

//MARK:- STORY BOARD NAMES
public let SB_LOGIN:String = "Login"
public let SB_TABBAR:String = "TabBar"
public let SB_HOME:String = "Home"
public let SB_PROFILE:String = "Profile"
public let SB_Question:String = "Question"
public let SB_Alerts:String = "Alerts"

//MARK:- USER DEFAULTS KEY
let UD_UserData:String = "UDUserData"
let UD_UserId:String = "UDUserId"
let UD_SecretLogID:String = "UDSecretLogID"
let UD_APIToken:String = "UDUserAccessToken"
let UD_DeviceToken:String = "UDDeviceToken"

//MARK:- CONTROLLERS ID
public let idNavMain = "navMain"
public let idNavHome = "tabnav"
public let idLoginVC = "LoginVC"
public let idRegisterVC = "RegisterVC"
public let idForgotPasswordVC = "ForgotPasswordVC"
public let idPasscodeVC = "PasscodeVC"
public let idTabBarVC = "TabBarVC"
public let idHomeVC = "HomeVC"
public let idQuestionVC = "QuestionVC"
public let idQuestionAnswerVC = "QuestionAnswerVC"
public let idSubjectListVC = "SubjectListVC"
public let idQuestionAnsweredVC = "QuestionAnsweredVC"
public let idMyProgressVC = "MyProgressVC"
public let idSWRevealViewController = "SWRevealViewController"
public let idChangePasswordVC = "ChangePasswordVC"
public let idEditProfileVC = "EditProfileVC"
public let idSubmitedAnsVC = "SubmitedAnsVC"
public let idExplanationVC = "ExplanationVC"
public let idAddGoalVC = "AddGoalVC"
public let idShareAppVC = "ShareAppVC"
public let idReferedFriendVC = "ReferedFriendVC"
public let idSelectModeVC = "SelectModeVC"
public let idSkillDetailsVC = "SkillDetailsVC"
public let idPaymentInfoVC = "PaymentInfoVC"
public let idSubscriptionDetailsVC = "SubscriptionDetailsVC"
public let idPaymentVC = "PaymentVC"
public let idVideoPlayVC = "VideoPlayVC"


//MARK - MESSAGES
let ServerResponseError = "Server Response Error"
let EnterFname = "First Name is required."
let EnterLname = "Last Name is required."
let EnterEmail = "Email address is required."
let EnterValidEmail = "Enter valid email address."
let EnterPhoneNo = "Phone number is required."
let EnterValidPhoneNo = "Enter valid phone number."
let EnterPassword = "Password is required."
let EnterConfirmPassword = "Confirm password is required."
let AgreeTerms = "Please accept terms and condition"
let EnterMobile = "Mobile number is required."
let EnterValidMobile = "Mobile number is required."
let EnterOLDPassword = "Old Password is required."
let EnterNEWPassword = "New Password is required."
let PasswordMismatch = "Password Mismatch."
let EnterMinLengthPassword = "Minimum length of password is 6 character."
let EnterBirthDate = "Birth date is required."
let EnterState = "State is required."
let DontHaveCamera = "You don't have camera."
let InternetNotAvail = "Internet Not Available."
let LocationEnabled = "Location Enabled"
let ErrorMSG = "Something going wrong. Please try again!"
