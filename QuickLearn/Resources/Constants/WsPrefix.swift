//
//  Constants.swift
//
//  Created by C237 on 30/10/17.
//  Copyright © 2017. All rights reserved.
//

import Foundation
import UIKit

let kDATA = "data"
let kSUCCESS = "success"
let kMESSAGE = "message"
let kRESPONSE_STATUS = "ResponseStatus"

let SERVER_URL = "http://www.learnonthego.in/Client/WebService/service"
let UPLOAD_IMAGE_URL = "http://www.learnonthego.in/Client//upload/EditClientImage"
let TERMS_CONDITION = ""

let DeviceType = "1"

//API Services
let APILogin = "login"
let APIRegister = "clientregister"
let APISocialLogin = "sociallogin"
let APIForgot = "forgot"
let APIVerifyOTP = "verifyotp"
let APIGetCourse = "getcourse"
let APIGetSubject = "getsubjectlistbycourse"
let APIQuesAns = "getquestionanswer"
let APIChangePass = "changepassword"
let APIGetState = "getstate"
let APIEditProfile = "editclientprofile"
let APISaveAnswer = "save_answer"
let APIAttempt = "attempt"
let APIGetResult = "getresult"
let APIGetProgress = "get_progress"
let APIGetReference = "get_reference"
let APISetGoal = "set_goal"
let APIGetUserDetails = "get_userdetail"
let APIPayment = "payment_request"
let APIPaymentStatus = "payment_status"
let APIGetSubscription = "get_subscription"
let APIGetSubjectResult = "get_subject_result"
let APIRestartCourse = "restart_course"
let APILogout = "logout"

